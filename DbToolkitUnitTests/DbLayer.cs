﻿using Byte.Toolkit.Db;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace DbToolkitUnitTests
{
    internal class DbLayer
    {
        public static void Init()
        {
            SingleDbManager.RegisterDbObjectType(typeof(User));
            SingleDbManager.AddQueriesFile(typeof(User), @"data\UserQueries.xml");
            SingleDbManager.RegisterDbObjectType(typeof(UserGroup));
            SingleDbManager.AddQueriesFile(typeof(UserGroup), @"data\UserGroupQueries.xml");
        }

        public DbLayer()
        {
            DbProviderFactories.RegisterFactory("System.Data.SQLite", "System.Data.SQLite.SQLiteFactory, System.Data.SQLite");
            DbManager = new SingleDbManager(@"Data Source=data\testdb.sqlite", "System.Data.SQLite");

            UserGroup = new UserGroupLayer(DbManager);
            User = new UserLayer(DbManager);
        }

        public SingleDbManager DbManager { get; set; }
        public UserGroupLayer UserGroup { get; set; }
        public UserLayer User { get; set; }
    }

    internal class UserGroupLayer : SingleDbObjectLayer<UserGroup>
    {
        public UserGroupLayer(SingleDbManager db)
            : base(db) { }

        public List<UserGroup> GetAllGroups() => DbManager.FillObjects<UserGroup>(Queries["GetAllGroups"]);
    }

    internal class UserLayer : SingleDbObjectLayer<User>
    {
        public UserLayer(SingleDbManager db)
            : base(db) { }

        public List<User> GetAllUsers() => DbManager.FillObjects<User>(Queries["GetAllUsers"]);

        public User? GetUserById(Int64 id)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            parameters.Add(DbManager.CreateParameter("id", id));
            return DbManager.FillSingleObject<User>(Queries["GetUserById"], CommandType.Text, parameters);
        }
    }
}
