﻿using Byte.Toolkit.Db;
using Byte.Toolkit.Db.Databases;
using DbToolkitUnitTests.MultiDbManagerTests.Database.Objects;
using System.Data.Common;
using System.Reflection;
using System.IO;

namespace DbToolkitUnitTests.MultiDbManagerTests.Database
{
    public class MyDatabase
    {
        public static void Init(string queriesPath)
        {
            //MultiDbManager.RegisterDbObjectTypes(Assembly.GetAssembly(typeof(Category)));

            MultiDbManager.RegisterDatabasesTypes(typeof(SQLiteDatabase));
            MultiDbManager.RegisterDbObjectType(typeof(Category));
            MultiDbManager.AddQueriesFile(typeof(Category), Path.Combine(queriesPath, "Category.xml"));
            MultiDbManager.RegisterDbObjectType(typeof(Product));
            MultiDbManager.AddQueriesFile(typeof(Product), Path.Combine(queriesPath, "Product.xml"));
        }

        public MyDatabase()
        {
            DbProviderFactories.RegisterFactory("System.Data.SQLite", "System.Data.SQLite.SQLiteFactory, System.Data.SQLite");
            DbManager = new MultiDbManager<SQLiteDatabase>(@"Data Source=MultiDbManagerTests\Database\my_db.sqlite", "System.Data.SQLite");

            Category = new CategoryLayer(DbManager);
            Product = new ProductLayer(DbManager);
        }

        public MultiDbManager DbManager { get; set; }
        public CategoryLayer Category { get; set; }
        public ProductLayer Product { get; set; }
    }
}
