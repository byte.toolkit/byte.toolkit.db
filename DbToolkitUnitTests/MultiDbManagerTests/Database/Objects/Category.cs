﻿using Byte.Toolkit.Db;
using Byte.Toolkit.Db.Attributes;
using Byte.Toolkit.Db.Converters;
using Byte.Toolkit.Db.Databases;
using System;

namespace DbToolkitUnitTests.MultiDbManagerTests.Database.Objects
{
    [Table("dbCategory")]
    public class Category
    {
        [Column("IdCategory")]
        [Converter(typeof(SQLiteDatabase), typeof(Int32ToInt64Converter))]
        public int IdCategory { get; set; }

        [Column("dbName")]
        public string? Name { get; set; }
    }
}
