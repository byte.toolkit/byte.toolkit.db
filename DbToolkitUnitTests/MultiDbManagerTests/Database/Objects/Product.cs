﻿using Byte.Toolkit.Db;
using Byte.Toolkit.Db.Attributes;
using Byte.Toolkit.Db.Converters;
using Byte.Toolkit.Db.Databases;
using System;

namespace DbToolkitUnitTests.MultiDbManagerTests.Database.Objects
{
    [Table("Product")]
    public class Product
    {
        [Column("IdProduct")]
        public string? IdProduct { get; set; }

        [Column("IdCategory")]
        [Converter(typeof(SQLiteDatabase), typeof(Int32ToInt64Converter))]
        public int IdCategory { get; set; }

        [Column("Name")]
        public string? Name { get; set; }

        [Column("Price")]
        public double? Price { get; set; }

        [Column("Created")]
        [Converter(typeof(SQLiteDatabase), typeof(DateTimeToStringConverter))]
        public DateTime? Created { get; set; }
    }
}
