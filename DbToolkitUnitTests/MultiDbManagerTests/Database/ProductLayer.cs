﻿using Byte.Toolkit.Db;
using DbToolkitUnitTests.MultiDbManagerTests.Database.Objects;
using System.Collections.Generic;
using System.Data.Common;

namespace DbToolkitUnitTests.MultiDbManagerTests.Database
{
    public class ProductLayer : MultiDbObjectLayer<Product>
    {
        public ProductLayer(MultiDbManager db)
            : base(db) { }

        public List<Product> SelectAllProducts() => DbManager.FillObjects<Product>(Queries["SelectAllProducts"]);

        public Product? SelectProductById(string idProduct)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            parameters.Add(DbManager.CreateParameter("idProduct", idProduct));
            return DbManager.FillSingleObject<Product>(Queries["SelectProductById"], parameters: parameters);
        }

        public int InsertProduct(Product product)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            parameters.Add(DbManager.CreateParameter("idProduct", product.IdProduct));
            parameters.Add(DbManager.CreateParameter("idCategory", product.IdCategory));
            parameters.Add(DbManager.CreateParameter("name", product.Name));
            parameters.Add(DbManager.CreateParameter("price", product.Price));
            parameters.Add(DbManager.CreateParameter("created", product.Created));
            return DbManager.ExecuteNonQuery(Queries["InsertProduct"], parameters: parameters);
        }

        public int UpdateProduct(Product product)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            parameters.Add(DbManager.CreateParameter("idProduct", product.IdProduct));
            parameters.Add(DbManager.CreateParameter("idCategory", product.IdCategory));
            parameters.Add(DbManager.CreateParameter("name", product.Name));
            parameters.Add(DbManager.CreateParameter("price", product.Price));
            parameters.Add(DbManager.CreateParameter("created", product.Created));
            return DbManager.ExecuteNonQuery(Queries["UpdateProduct"], parameters: parameters);
        }

        public int DeleteProductById(string idProduct)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            parameters.Add(DbManager.CreateParameter("idProduct", idProduct));
            return DbManager.ExecuteNonQuery(Queries["DeleteProductById"], parameters: parameters);
        }
    }
}
