﻿using Byte.Toolkit.Db;
using DbToolkitUnitTests.MultiDbManagerTests.Database.Objects;
using System.Collections.Generic;
using System.Data.Common;

namespace DbToolkitUnitTests.MultiDbManagerTests.Database
{
    public class CategoryLayer : MultiDbObjectLayer<Category>
    {
        public CategoryLayer(MultiDbManager db)
            : base(db) { }

        public List<Category> SelectAllCategorys() => DbManager.FillObjects<Category>(Queries["SelectAllCategorys"]);

        public Category? SelectCategoryById(int idCategory)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            parameters.Add(DbManager.CreateParameter("idCategory", idCategory));
            return DbManager.FillSingleObject<Category>(Queries["SelectCategoryById"], parameters: parameters);
        }

        public int InsertCategory(Category category)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            parameters.Add(DbManager.CreateParameter("idCategory", category.IdCategory));
            parameters.Add(DbManager.CreateParameter("name", category.Name));
            return DbManager.ExecuteNonQuery(Queries["InsertCategory"], parameters: parameters);
        }

        public int UpdateCategory(Category category)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            parameters.Add(DbManager.CreateParameter("idCategory", category.IdCategory));
            parameters.Add(DbManager.CreateParameter("name", category.Name));
            return DbManager.ExecuteNonQuery(Queries["UpdateCategory"], parameters: parameters);
        }

        public int DeleteCategoryById(int idCategory)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            parameters.Add(DbManager.CreateParameter("idCategory", idCategory));
            return DbManager.ExecuteNonQuery(Queries["DeleteCategoryById"], parameters: parameters);
        }
    }
}
