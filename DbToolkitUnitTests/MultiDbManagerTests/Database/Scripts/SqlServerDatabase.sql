﻿CREATE TABLE [dbCategory] (
    [IdCategory] INT NOT NULL,
    [dbName] NVARCHAR(50) NOT NULL,
    CONSTRAINT [PK_dbCategory_IdCategory] PRIMARY KEY ([IdCategory])
);

GO

CREATE UNIQUE INDEX [UK_Category_dbName] on [dbCategory] ([dbName]);

GO

CREATE TABLE [Product] (
    [IdProduct] NVARCHAR(50) NOT NULL,
    [IdCategory] INT NOT NULL,
    [Name] NVARCHAR(50) NOT NULL,
    [Price] FLOAT,
    [Created] DATETIME2,
    CONSTRAINT [PK_Product_IdProduct] PRIMARY KEY ([IdProduct]),
    CONSTRAINT [FK_Category_Product] FOREIGN KEY ([IdCategory]) REFERENCES [dbCategory] ([IdCategory])
);

GO

CREATE UNIQUE INDEX [UK_Product_Name] on [Product] ([Name]);

GO

