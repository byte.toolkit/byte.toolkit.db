﻿CREATE TABLE dbCategory (
    IdCategory INTEGER NOT NULL,
    dbName TEXT NOT NULL,
    PRIMARY KEY (IdCategory)
);

CREATE UNIQUE INDEX UK_Category_dbName on dbCategory (dbName);

CREATE TABLE Product (
    IdProduct TEXT NOT NULL,
    IdCategory INTEGER NOT NULL,
    Name TEXT NOT NULL,
    Price REAL,
    Created TEXT,
    PRIMARY KEY (IdProduct),
    FOREIGN KEY (IdCategory) REFERENCES dbCategory (IdCategory)
);

CREATE UNIQUE INDEX UK_Product_Name on Product (Name);

