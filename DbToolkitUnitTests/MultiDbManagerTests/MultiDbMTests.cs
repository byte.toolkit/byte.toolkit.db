﻿using System;
using System.Collections.Generic;
using System.Text;
using DbToolkitUnitTests.MultiDbManagerTests.Database;
using DbToolkitUnitTests.MultiDbManagerTests.Database.Objects;
using NUnit.Framework;

namespace DbToolkitUnitTests.MultiDbManagerTests
{
    internal class MultiDbMTests
    {
        static MultiDbMTests()
        {
            MyDatabase.Init(@"MultiDbManagerTests\Database\Queries");
        }

        [Test]
        public void ConnectionTest()
        {
            Assert.DoesNotThrow(() =>
            {
                MyDatabase db = new MyDatabase();
                db.DbManager.Open();

                db.DbManager.Close();
            });
        }

        [Test]
        public void SelectCategoriesTest()
        {
            MyDatabase db = new MyDatabase();
            db.DbManager.Open();

            List<Category> list = db.Category.SelectAllCategorys();

            Assert.AreEqual(2, list.Count);
            Assert.AreEqual("Libraries", list[0].Name);
            Assert.AreEqual("Apps", list[1].Name);

            db.DbManager.Close();
        }

        [Test]
        public void InsertUpdateDeleteTest()
        {
            MyDatabase db = new MyDatabase();
            db.DbManager.Open();

            int affectedLines = db.Category.InsertCategory(new Category()
            {
                IdCategory = 3,
                Name = "Test category"
            });

            Assert.AreEqual(1, affectedLines);

            Category? category = db.Category.SelectCategoryById(3);
            Assert.That(category != null);
            Assert.That(category.Name == "Test category");

            category.Name = "Blah blah";
            affectedLines = db.Category.UpdateCategory(category);
            Assert.AreEqual(1, affectedLines);

            category = db.Category.SelectCategoryById(3);
            Assert.AreEqual("Blah blah", category.Name);

            affectedLines = db.Category.DeleteCategoryById(3);
            Assert.AreEqual(1, affectedLines);

            category = db.Category.SelectCategoryById(3);
            Assert.That(category == null);

            db.DbManager.Close();
        }
    }
}
