﻿using Byte.Toolkit.Db.Attributes;
using System;

namespace DbToolkitUnitTests
{
    [Table]
    internal class UserGroup
    {
        [Column("GROUP_ID")]
        public Int64? Id { get; set; }

        [Column("GROUP_NAME")]
        public string? Name { get; set; }
    }

    [Table]
    internal class User
    {
        [Column("USER_ID")]
        public Int64? Id { get; set; }

        [Column("GROUP_ID")]
        public Int64? GroupId { get; set; }

        [Column("USERNAME")]
        public string? Username { get; set; }

        [Column("PASSWORD")]
        public string? Password { get; set; }

        [Column("FULL_NAME")]
        public string? Name { get; set; }
    }

    internal class TestObject
    {
        [Column("Name")]
        public string? Name { get; set; }
    }
}
