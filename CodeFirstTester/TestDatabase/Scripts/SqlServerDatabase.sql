﻿CREATE TABLE [UserGroup] (
    [IdGroup] INT NOT NULL,
    [Name] NVARCHAR(50) NOT NULL,
    CONSTRAINT [PK_UserGroup_IdGroup] PRIMARY KEY ([IdGroup])
);

GO

CREATE UNIQUE INDEX [UK_UserGroup_Name] on [UserGroup] ([Name]);

GO

CREATE TABLE [User] (
    [IdUser] INT NOT NULL,
    [IdGroup] INT NOT NULL,
    [Username] NVARCHAR(50) NOT NULL,
    [Password] NVARCHAR(50) NOT NULL,
    [Created] DATETIME2,
    [Height] DECIMAL(18,0),
    CONSTRAINT [PK_User_IdUser] PRIMARY KEY ([IdUser]),
    CONSTRAINT [FK_UserGroup_User_IdGroup] FOREIGN KEY ([IdGroup]) REFERENCES [UserGroup] ([IdGroup])
);

GO

CREATE INDEX [IDX_User_Created] on [User] ([Created]);

GO

CREATE UNIQUE INDEX [UK_User_Username] on [User] ([Username]);

GO

CREATE TABLE [Test] (
    [ID] INT NOT NULL
);

GO

