﻿CREATE TABLE UserGroup (
    IdGroup INTEGER NOT NULL,
    Name TEXT NOT NULL,
    PRIMARY KEY (IdGroup)
);

CREATE UNIQUE INDEX UK_UserGroup_Name on UserGroup (Name);

CREATE TABLE User (
    IdUser INTEGER NOT NULL,
    IdGroup INTEGER NOT NULL,
    Username TEXT NOT NULL,
    Password TEXT NOT NULL,
    Created TEXT,
    Height TEXT,
    PRIMARY KEY (IdUser),
    FOREIGN KEY (IdGroup) REFERENCES UserGroup (IdGroup)
);

CREATE INDEX IDX_User_Created on User (Created);
CREATE UNIQUE INDEX UK_User_Username on User (Username);

CREATE TABLE Test (
    ID INTEGER NOT NULL
);


