﻿using Byte.Toolkit.Db;
using MyNamespace.Objects;
using System.Collections.Generic;
using System.Data.Common;

namespace MyNamespace
{
    public class UserGroupLayer : MultiDbObjectLayer<UserGroup>
    {
        public UserGroupLayer(MultiDbManager db)
            : base(db) { }

        public List<UserGroup> SelectAllUserGroups() => DbManager.FillObjects<UserGroup>(Queries["SelectAllUserGroups"]);

        public UserGroup? SelectUserGroupById(int idGroup)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            parameters.Add(DbManager.CreateParameter("idGroup", idGroup));
            return DbManager.FillSingleObject<UserGroup>(Queries["SelectUserGroupById"], parameters: parameters);
        }

        public int InsertUserGroup(UserGroup userGroup)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            parameters.Add(DbManager.CreateParameter("idGroup", userGroup.IdGroup));
            parameters.Add(DbManager.CreateParameter("name", userGroup.Name));
            return DbManager.ExecuteNonQuery(Queries["InsertUserGroup"], parameters: parameters);
        }

        public int UpdateUserGroup(UserGroup userGroup)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            parameters.Add(DbManager.CreateParameter("idGroup", userGroup.IdGroup));
            parameters.Add(DbManager.CreateParameter("name", userGroup.Name));
            return DbManager.ExecuteNonQuery(Queries["UpdateUserGroup"], parameters: parameters);
        }

        public int DeleteUserGroupById(int idGroup)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            parameters.Add(DbManager.CreateParameter("idGroup", idGroup));
            return DbManager.ExecuteNonQuery(Queries["DeleteUserGroupById"], parameters: parameters);
        }
    }
}
