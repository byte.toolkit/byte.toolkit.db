﻿using Byte.Toolkit.Db;
using MyNamespace.Objects;
using System.Collections.Generic;
using System.Data.Common;

namespace MyNamespace
{
    public class TestLayer : MultiDbObjectLayer<Test>
    {
        public TestLayer(MultiDbManager db)
            : base(db) { }

        public List<Test> SelectAllTests() => DbManager.FillObjects<Test>(Queries["SelectAllTests"]);

        public Test? SelectTestById(int iD)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            parameters.Add(DbManager.CreateParameter("iD", iD));
            return DbManager.FillSingleObject<Test>(Queries["SelectTestById"], parameters: parameters);
        }

        public int InsertTest(Test test)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            parameters.Add(DbManager.CreateParameter("iD", test.ID));
            return DbManager.ExecuteNonQuery(Queries["InsertTest"], parameters: parameters);
        }

        public int UpdateTest(Test test)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            parameters.Add(DbManager.CreateParameter("iD", test.ID));
            return DbManager.ExecuteNonQuery(Queries["UpdateTest"], parameters: parameters);
        }

        public int DeleteTestById(int iD)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            parameters.Add(DbManager.CreateParameter("iD", iD));
            return DbManager.ExecuteNonQuery(Queries["DeleteTestById"], parameters: parameters);
        }
    }
}
