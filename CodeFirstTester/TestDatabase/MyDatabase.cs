﻿using Byte.Toolkit.Db;
using Byte.Toolkit.Db.Databases;
using MyNamespace.Objects;
using System.Data.Common;

namespace MyNamespace
{
    public class MyDatabase
    {
        public MyDatabase()
        {
            DbProviderFactories.RegisterFactory("System.Data.SQLite", "System.Data.SQLite.SQLiteFactory, System.Data.SQLite");
            DbManager = new MultiDbManager<SQLiteDatabase>(@"Data Source=C:\Temp\my_db.sqlite", "System.Data.SQLite");
            MultiDbManager.RegisterDatabasesTypes(typeof(SQLiteDatabase));

            MultiDbManager.RegisterDbObjectType(typeof(UserGroup));
            MultiDbManager.AddQueriesFile(typeof(UserGroup), @"TestDatabase\Queries\UserGroup.xml");
            UserGroup = new UserGroupLayer(DbManager);

            MultiDbManager.RegisterDbObjectType(typeof(User));
            MultiDbManager.AddQueriesFile(typeof(User), @"TestDatabase\Queries\User.xml");
            User = new UserLayer(DbManager);

            MultiDbManager.RegisterDbObjectType(typeof(Test));
            MultiDbManager.AddQueriesFile(typeof(Test), @"TestDatabase\Queries\Test.xml");
            Test = new TestLayer(DbManager);
        }

        public MultiDbManager DbManager { get; set; }
        public UserGroupLayer UserGroup { get; set; }
        public UserLayer User { get; set; }
        public TestLayer Test { get; set; }
    }
}
