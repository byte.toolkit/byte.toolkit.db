﻿using Byte.Toolkit.Db;
using Byte.Toolkit.Db.Attributes;
using Byte.Toolkit.Db.Converters;
using Byte.Toolkit.Db.Databases;
using System;

namespace MyNamespace.Objects
{
    [Table("UserGroup")]
    public class UserGroup
    {
        [Column("IdGroup")]
        [Converter(typeof(SQLiteDatabase), typeof(Int32ToInt64Converter))]
        public int IdGroup { get; set; }

        [Column("Name")]
        public string Name { get; set; }
    }
}
