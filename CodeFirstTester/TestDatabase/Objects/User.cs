﻿using Byte.Toolkit.Db;
using Byte.Toolkit.Db.Attributes;
using Byte.Toolkit.Db.Converters;
using Byte.Toolkit.Db.Databases;
using System;

namespace MyNamespace.Objects
{
    [Table("User")]
    public class User
    {
        [Column("IdUser")]
        [Converter(typeof(SQLiteDatabase), typeof(Int32ToInt64Converter))]
        public int IdUser { get; set; }

        [Column("IdGroup")]
        [Converter(typeof(SQLiteDatabase), typeof(Int32ToInt64Converter))]
        public int IdGroup { get; set; }

        [Column("Username")]
        public string Username { get; set; }

        [Column("Password")]
        public string Password { get; set; }

        [Column("Created")]
        [Converter(typeof(SQLiteDatabase), typeof(DateTimeToStringConverter))]
        public DateTime? Created { get; set; }

        [Column("Height")]
        [Converter(typeof(SQLiteDatabase), typeof(DecimalToStringConverter))]
        public decimal? Height { get; set; }
    }
}
