﻿using Byte.Toolkit.Db;
using Byte.Toolkit.Db.Attributes;
using Byte.Toolkit.Db.Converters;
using Byte.Toolkit.Db.Databases;
using System;

namespace MyNamespace.Objects
{
    [Table("Test")]
    public class Test
    {
        [Column("ID")]
        [Converter(typeof(SQLiteDatabase), typeof(Int32ToInt64Converter))]
        public int ID { get; set; }
    }
}
