﻿using Byte.Toolkit.Db;
using MyNamespace.Objects;
using System.Collections.Generic;
using System.Data.Common;

namespace MyNamespace
{
    public class UserLayer : MultiDbObjectLayer<User>
    {
        public UserLayer(MultiDbManager db)
            : base(db) { }

        public List<User> SelectAllUsers() => DbManager.FillObjects<User>(Queries["SelectAllUsers"]);

        public User? SelectUserById(int idUser)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            parameters.Add(DbManager.CreateParameter("idUser", idUser));
            return DbManager.FillSingleObject<User>(Queries["SelectUserById"], parameters: parameters);
        }

        public int InsertUser(User user)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            parameters.Add(DbManager.CreateParameter("idUser", user.IdUser));
            parameters.Add(DbManager.CreateParameter("idGroup", user.IdGroup));
            parameters.Add(DbManager.CreateParameter("username", user.Username));
            parameters.Add(DbManager.CreateParameter("password", user.Password));
            parameters.Add(DbManager.CreateParameter("created", user.Created));
            parameters.Add(DbManager.CreateParameter("height", user.Height));
            return DbManager.ExecuteNonQuery(Queries["InsertUser"], parameters: parameters);
        }

        public int UpdateUser(User user)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            parameters.Add(DbManager.CreateParameter("idUser", user.IdUser));
            parameters.Add(DbManager.CreateParameter("idGroup", user.IdGroup));
            parameters.Add(DbManager.CreateParameter("username", user.Username));
            parameters.Add(DbManager.CreateParameter("password", user.Password));
            parameters.Add(DbManager.CreateParameter("created", user.Created));
            parameters.Add(DbManager.CreateParameter("height", user.Height));
            return DbManager.ExecuteNonQuery(Queries["UpdateUser"], parameters: parameters);
        }

        public int DeleteUserById(int idUser)
        {
            List<DbParameter> parameters = new List<DbParameter>();
            parameters.Add(DbManager.CreateParameter("idUser", idUser));
            return DbManager.ExecuteNonQuery(Queries["DeleteUserById"], parameters: parameters);
        }
    }
}
