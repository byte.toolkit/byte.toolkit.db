﻿using Byte.Toolkit.Db.Databases;
using Byte.Toolkit.Db.Generators;
using MyNamespace;
using MyNamespace.Objects;
using System;
using System.Collections.Generic;
using System.IO;

namespace CodeFirstTester
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                GenerateDatabase(@"C:\Temp\MultiDbOutput");
                //MyDatabase db = new MyDatabase();
                //db.DbManager.Open();

                ////db.UserGroup.InsertUserGroup(new UserGroup()
                ////{
                ////    IdGroup = 1,
                ////    Name = "Administrators"
                ////});

                ////db.UserGroup.InsertUserGroup(new UserGroup()
                ////{
                ////    IdGroup = 2,
                ////    Name = "Users"
                ////});

                //List<UserGroup> groups = db.UserGroup.SelectAllUserGroups();

                ////db.User.InsertUser(new User()
                ////{
                ////    IdUser = 1,
                ////    IdGroup = 1,
                ////    Username = "admin",
                ////    Password = "admin1234",
                ////    Created = DateTime.Now,
                ////    Height = 10.0M
                ////});

                ////db.User.InsertUser(new User()
                ////{
                ////    IdUser = 2,
                ////    IdGroup = 1,
                ////    Username = "admin2",
                ////    Password = "admin21234"
                ////});

                //List<User> users = db.User.SelectAllUsers();

                //db.DbManager.Close();
            }
            catch (Exception ex)
            {

            }
        }

        private static void GenerateDatabase(string output)
        {
            MultiDbGenerator generator = new MultiDbGenerator(SQLiteDatabase.Instance, SqlServerDatabase.Instance);

            MultiDbTable categoriesTable = new MultiDbTable("dbCategory", "Category",
                new PrimaryKeyConstraint("IdCategory", "PK_dbCategory_IdCategory"),
                new UniqueKeyConstraint("dbName", "UK_Category_dbName")
            );
            categoriesTable.Columns.Add(new MultiDbColumn<int>("IdCategory"));
            categoriesTable.Columns.Add(new MultiDbColumn<string?>("dbName", "Name"));

            MultiDbTable productsTable = new MultiDbTable("Product",
                new PrimaryKeyConstraint("IdProduct", "PK_Product_IdProduct"),
                new UniqueKeyConstraint("Name", "UK_Product_Name"),
                new ForeignKeyConstraint("IdCategory", "dbCategory", "IdCategory", "FK_Category_Product")
            );
            productsTable.Columns.Add(new MultiDbColumn<string?>("IdProduct"));
            productsTable.Columns.Add(new MultiDbColumn<int>("IdCategory"));
            productsTable.Columns.Add(new MultiDbColumn<string?>("Name"));
            productsTable.Columns.Add(new MultiDbColumn<double?>("Price", isNullable: true));
            productsTable.Columns.Add(new MultiDbColumn<DateTime?>("Created", isNullable: true));
            
            generator.Tables.Add(categoriesTable);
            generator.Tables.Add(productsTable);


            MultiDbGeneratorConfig config = new MultiDbGeneratorConfig()
            {
                PropertyType = PropertyType.GetSet,
                LayersNamespace = "DbToolkitUnitTests.MultiDbManagerTests.Database",
                ClassesNamespace = "DbToolkitUnitTests.MultiDbManagerTests.Database.Objects"
            };


            string scriptsPath = Path.Combine(output, "Scripts");
            string objectsPath = Path.Combine(output, "Objects");
            string queriesPath = Path.Combine(output, "Queries");

            if (!Directory.Exists(output))
                Directory.CreateDirectory(output);
            if (!Directory.Exists(scriptsPath))
                Directory.CreateDirectory(scriptsPath);
            if (!Directory.Exists(objectsPath))
                Directory.CreateDirectory(objectsPath);
            if (!Directory.Exists(queriesPath))
                Directory.CreateDirectory(queriesPath);

            generator.GenerateDatabasesCreationScripts(scriptsPath);
            generator.GenerateClassesFiles(objectsPath, config);
            generator.GenerateQueriesFiles(queriesPath, config);
            generator.GenerateDatabaseLayerFile(output, config);
            generator.GenerateClassLayersFiles(output, config);
        }
    }
}
