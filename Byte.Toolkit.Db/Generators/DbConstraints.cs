﻿using System;

namespace Byte.Toolkit.Db.Generators
{
    /// <summary>
    /// Base class for database constraints
    /// </summary>
    public abstract class DbConstraint
    {
        /// <summary>
        /// Constructor for <see cref="DbConstraint"/>
        /// </summary>
        /// <param name="columns">Columns separated with comma</param>
        /// <param name="constraintName">Constraint name</param>
        /// <exception cref="ArgumentNullException"></exception>
        public DbConstraint(string columns, string constraintName)
        {
            if (columns == null)
                throw new ArgumentNullException(nameof(columns));

            if (constraintName == null)
                throw new ArgumentNullException(nameof(constraintName));

            Columns = columns;
            ConstraintName = constraintName;
        }

        /// <summary>
        /// Columns used for constraints, separated with comma
        /// </summary>
        public string Columns { get; set; }

        /// <summary>
        /// Constraint name
        /// </summary>
        public string ConstraintName { get; set; }

        /// <summary>
        /// Is constraint generated inside the table creation declaration
        /// </summary>
        public abstract bool InsideTableDeclaration { get; }
    }

    /// <summary>
    /// Primary key constraint
    /// </summary>
    public class PrimaryKeyConstraint : DbConstraint
    {
        /// <summary>
        /// Constructor for <see cref="PrimaryKeyConstraint"/>
        /// </summary>
        /// <param name="columns">Columns separated with comma</param>
        /// <param name="constraintName"></param>
        public PrimaryKeyConstraint(string columns, string constraintName)
            : base(columns, constraintName)
        { }

        /// <inheritdoc/>
        public override bool InsideTableDeclaration => true;
    }

    /// <summary>
    /// Unique key constraint
    /// </summary>
    public class UniqueKeyConstraint : DbConstraint
    {
        /// <summary>
        /// Constructor for <see cref="UniqueKeyConstraint"/>
        /// </summary>
        /// <param name="columns">Columns separated with comma</param>
        /// <param name="constraintName">Constraint name</param>
        public UniqueKeyConstraint(string columns, string constraintName)
            : base(columns, constraintName)
        { }

        /// <inheritdoc/>
        public override bool InsideTableDeclaration => false;
    }

    /// <summary>
    /// Index constraint
    /// </summary>
    public class IndexConstraint : DbConstraint
    {
        /// <summary>
        /// Constructor for <see cref="IndexConstraint"/>
        /// </summary>
        /// <param name="columns">Columns separated with comma</param>
        /// <param name="constraintName">Constraint name</param>
        public IndexConstraint(string columns, string constraintName)
            : base(columns, constraintName)
        { }

        /// <inheritdoc/>
        public override bool InsideTableDeclaration => false;
    }

    /// <summary>
    /// Foreign key constraint
    /// </summary>
    public class ForeignKeyConstraint : DbConstraint
    {
        /// <summary>
        /// Constructor for <see cref="ForeignKeyConstraint"/>
        /// </summary>
        /// <param name="columns">Columns separated with comma</param>
        /// <param name="foreignTableName">Foreign table name</param>
        /// <param name="foreignColumnName">Foreign column name</param>
        /// <param name="constraintName">Constraint name</param>
        /// <exception cref="ArgumentNullException"></exception>
        public ForeignKeyConstraint(string columns, string foreignTableName, string foreignColumnName, string constraintName)
            : base(columns, constraintName)
        {
            if (foreignTableName == null)
                throw new ArgumentNullException(nameof(foreignTableName));
            if (foreignColumnName == null)
                throw new ArgumentNullException(nameof(foreignColumnName));

            ForeignTableName = foreignTableName;
            ForeignColumnName = foreignColumnName;
        }

        /// <summary>
        /// Foreign table name
        /// </summary>
        public string ForeignTableName { get; set; }

        /// <summary>
        /// Foreign column name
        /// </summary>
        public string ForeignColumnName { get; set; }

        /// <inheritdoc/>
        public override bool InsideTableDeclaration => true;
    }
}
