﻿using System;

namespace Byte.Toolkit.Db.Generators
{
    /// <summary>
    /// Column class for code generation for <see cref="MultiDbManager{TDatabase}"/>
    /// </summary>
    public abstract class MultiDbColumn
    {
        /// <summary>
        /// Column name
        /// </summary>
        public abstract string ColumnName { get; set; }

        /// <summary>
        /// Is column nullable
        /// </summary>
        public abstract bool IsNullable { get; set; }

        /// <summary>
        /// Property name
        /// </summary>
        public abstract string PropertyName { get; set; }

        /// <summary>
        /// Raw property type
        /// </summary>
        public abstract Type RawPropertyType { get; set; }

        /// <summary>
        /// Property nullable underlying type
        /// </summary>
        public abstract Type? NullableUnderlyingPropertyType { get; set; }

        /// <summary>
        /// Is property type nullable
        /// </summary>
        public abstract bool IsPropertyTypeNullable { get; set; }

        /// <summary>
        /// Property type
        /// </summary>
        public abstract Type PropertyType { get; set; }
    }

    /// <summary>
    /// Generic column class for code generation for <see cref="MultiDbManager{TDatabase}"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MultiDbColumn<T> : MultiDbColumn
    {
        /// <summary>
        /// Constructor with the same name for column and property
        /// </summary>
        /// <param name="name">Column and property name</param>
        /// <param name="isNullable">Is column nullable</param>
        public MultiDbColumn(string name, bool isNullable = false)
        {
            ColumnName = name;
            IsNullable = isNullable;
            PropertyName = name;
            RawPropertyType = typeof(T);
            NullableUnderlyingPropertyType = Nullable.GetUnderlyingType(RawPropertyType);
            IsPropertyTypeNullable = NullableUnderlyingPropertyType != null;
            PropertyType = NullableUnderlyingPropertyType ?? RawPropertyType;
        }

        /// <summary>
        /// Constructor with different names for column and property
        /// </summary>
        /// <param name="columnName">Column name</param>
        /// <param name="propertyName">Property name</param>
        /// <param name="isNullable">Is column nullable</param>
        public MultiDbColumn(string columnName, string propertyName, bool isNullable = false)
        {
            ColumnName = columnName;
            IsNullable = isNullable;
            PropertyName = propertyName;
            RawPropertyType = typeof(T);
            NullableUnderlyingPropertyType = Nullable.GetUnderlyingType(RawPropertyType);
            IsPropertyTypeNullable = NullableUnderlyingPropertyType != null;
            PropertyType = NullableUnderlyingPropertyType ?? RawPropertyType;
        }

        /// <inheritdoc/>
        public override string ColumnName { get; set; }

        /// <inheritdoc/>
        public override bool IsNullable { get; set; }

        /// <inheritdoc/>
        public override string PropertyName { get; set; }

        /// <inheritdoc/>
        public override Type RawPropertyType { get; set; }

        /// <inheritdoc/>
        public override Type? NullableUnderlyingPropertyType { get; set; }
        
        /// <inheritdoc/>
        public override bool IsPropertyTypeNullable { get; set; }

        /// <inheritdoc/>
        public override Type PropertyType { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"<{ColumnName}: {PropertyType}, {(IsNullable ? "NULLABLE" : "")}>";
        }
    }
}
