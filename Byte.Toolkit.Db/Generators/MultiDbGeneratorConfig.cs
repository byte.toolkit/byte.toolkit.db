﻿using System;

namespace Byte.Toolkit.Db.Generators
{
    /// <summary>
    /// Configuration class for code generation
    /// </summary>
    public class MultiDbGeneratorConfig
    {
        /// <summary>
        /// Classes namespace
        /// </summary>
        public string ClassesNamespace { get; set; } = "MyNamespace.Objects";

        /// <summary>
        /// Parent class
        /// </summary>
        public string ParentClass { get; set; } = "INotifyPropertyChanged";

        /// <summary>
        /// Parent class using namespace
        /// </summary>
        public string ParentClassUsing { get; set; } = "using System.ComponentModel;";

        /// <summary>
        /// Property type
        /// </summary>
        public PropertyType PropertyType { get; set; } = PropertyType.GetSet;

        /// <summary>
        /// Property set template for string format. {0} is field name, {1} is property name
        /// </summary>
        public string PropertySetTemplate { get; set; } = @"set
            {{
                {0} = value;
                OnPropertyChanged(nameof({1}));
            }}";

        /// <summary>
        /// Database layers namespace
        /// </summary>
        public string LayersNamespace { get; set; } = "MyNamespace";

        /// <summary>
        /// Main layer class name
        /// </summary>
        public string LayerClassName { get; set; } = "MyDatabase";

        /// <summary>
        /// Default provider name
        /// </summary>
        public string DefaultProviderName { get; set; } = "System.Data.SQLite";

        /// <summary>
        /// Default connection string
        /// </summary>
        public string DefaultConnectionString { get; set; } = "Data Source=my_db.sqlite";

        /// <summary>
        /// Default factory type
        /// </summary>
        public string DefaultFactoryType { get; set; } = "System.Data.SQLite.SQLiteFactory, System.Data.SQLite";

        /// <summary>
        /// Default database type
        /// </summary>
        public string DefaultDatabaseType { get; set; } = "SQLiteDatabase";
    }
}
