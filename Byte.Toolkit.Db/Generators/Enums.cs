﻿namespace Byte.Toolkit.Db.Generators
{
    /// <summary>
    /// Property type for class generation
    /// </summary>
    public enum PropertyType
    {
        /// <summary>
        /// Write properties with { get; set; }
        /// </summary>
        GetSet,

        /// <summary>
        /// Write properties with private fields to allow
        /// usage of Properties Notification
        /// </summary>
        GetSetWithFields
    }
}
