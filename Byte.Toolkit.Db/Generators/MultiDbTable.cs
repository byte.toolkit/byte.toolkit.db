﻿using System;
using System.Collections.Generic;

namespace Byte.Toolkit.Db.Generators
{
    /// <summary>
    /// Table class for code generation for <see cref="MultiDbManager{TDatabase}"/>
    /// </summary>
    public class MultiDbTable
    {
        /// <summary>
        /// Constructor with same name for table and class
        /// </summary>
        /// <param name="name">Table and class name</param>
        /// <param name="constraints">Database constraints</param>
        public MultiDbTable(string name, params DbConstraint[] constraints)
        {
            TableName = name;
            ClassName = name;
            Columns = new List<MultiDbColumn>();
            Constraints = new List<DbConstraint>(constraints);
        }

        /// <summary>
        /// Constructor with different names for table and class
        /// </summary>
        /// <param name="tableName">Table name</param>
        /// <param name="className">Class name</param>
        /// <param name="constraints">Database constraints</param>
        public MultiDbTable(string tableName, string className, params DbConstraint[] constraints)
        {
            TableName = tableName;
            ClassName = className;
            Columns = new List<MultiDbColumn>();
            Constraints = new List<DbConstraint>(constraints);
        }

        /// <summary>
        /// Table name
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// Table class name
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// Table columns
        /// </summary>
        public List<MultiDbColumn> Columns { get; set; }

        /// <summary>
        /// Table constraints
        /// </summary>
        public List<DbConstraint> Constraints { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"<{TableName}, {Columns.Count} columns>";
        }
    }
}
