﻿using Byte.Toolkit.Db.Converters;
using Byte.Toolkit.Db.Databases;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Byte.Toolkit.Db.Generators
{
    /// <summary>
    /// Code generator class for <see cref="MultiDbManager"/> and <see cref="MultiDbManager{TDatabase}"/>
    /// </summary>
    public class MultiDbGenerator
    {
        /// <summary>
        /// Constructor for <see cref="MultiDbGenerator"/>
        /// </summary>
        /// <param name="databases">Databases</param>
        public MultiDbGenerator(params IDatabase[] databases)
        {
            Tables = new List<MultiDbTable>();
            Databases = databases;
        }

        /// <summary>
        /// Tables list
        /// </summary>
        public List<MultiDbTable> Tables { get; }

        /// <summary>
        /// Databases
        /// </summary>
        public IDatabase[] Databases { get; }

        /// <summary>
        /// Simplified types for database script generation
        /// </summary>
        public static Dictionary<Type, string> SimplifiedTypes { get; } =
            new Dictionary<Type, string>
            {
                { typeof(string),  "string"  },
                { typeof(byte),    "byte"    },
                { typeof(byte[]),  "byte[]"  },
                { typeof(short),   "short"   },
                { typeof(ushort),  "ushort"  },
                { typeof(int),     "int"     },
                { typeof(uint),    "uint"    },
                { typeof(long),    "long"    },
                { typeof(ulong),   "ulong"   },
                { typeof(float),   "float"   },
                { typeof(double),  "double"  },
                { typeof(decimal), "decimal" }
            };

        /// <summary>
        /// Generate databases creation scripts
        /// </summary>
        /// <param name="output">SQL files output path</param>
        public void GenerateDatabasesCreationScripts(string output)
        {
            foreach(IDatabase db in Databases)
            {
                string filename = $"{db.GetType().Name}.sql";
                string file = Path.Combine(output, filename);

                using(FileStream fs = new FileStream(file, FileMode.Create, FileAccess.Write))
                {
                    using(StreamWriter sw = new StreamWriter(fs, Encoding.UTF8))
                    {
                        string code = db.GenerateDatabaseCreationScript(Tables);
                        sw.Write(code);
                    }
                }
            }
        }

        /// <summary>
        /// Generate classes files
        /// </summary>
        /// <param name="output">C# files output path</param>
        /// <param name="config">Generation configuration</param>
        public void GenerateClassesFiles(string output, MultiDbGeneratorConfig config)
        {
            foreach(MultiDbTable table in Tables)
            {
                string filename = $"{table.ClassName}.cs";
                string file = Path.Combine(output, filename);

                using(FileStream fs = new FileStream(file, FileMode.Create, FileAccess.Write))
                {
                    using(StreamWriter sw = new StreamWriter(fs, Encoding.UTF8))
                    {
                        string code = GenerateClass(table, config);
                        sw.Write(code);
                    }
                }
            }
        }

        /// <summary>
        /// Generate database layer file
        /// </summary>
        /// <param name="output">C# files output path</param>
        /// <param name="config">Generation configuration</param>
        public void GenerateDatabaseLayerFile(string output, MultiDbGeneratorConfig config)
        {
            string filename = $"{config.LayerClassName}.cs";
            string file = Path.Combine(output, filename);

            using (FileStream fs = new FileStream(file, FileMode.Create, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs, Encoding.UTF8))
                {
                    string code = GenerateDatabaseLayer(config);
                    sw.Write(code);
                }
            }
        }

        /// <summary>
        /// Generate classes layers files
        /// </summary>
        /// <param name="output">C# files output path</param>
        /// <param name="config">Generation configuration</param>
        public void GenerateClassLayersFiles(string output, MultiDbGeneratorConfig config)
        {
            foreach(MultiDbTable table in Tables)
            {
                string filename = $"{table.ClassName}Layer.cs";
                string file = Path.Combine(output, filename);

                using (FileStream fs = new FileStream(file, FileMode.Create, FileAccess.Write))
                {
                    using (StreamWriter sw = new StreamWriter(fs, Encoding.UTF8))
                    {
                        string code = GenerateClassLayer(table, config);
                        sw.Write(code);
                    }
                }
            }
        }

        /// <summary>
        /// Generate queries files
        /// </summary>
        /// <param name="output">XML files output path</param>
        /// <param name="config">Generation configuration</param>
        /// <param name="parameterPrefix">Parameter prefix</param>
        public void GenerateQueriesFiles(string output, MultiDbGeneratorConfig config, char parameterPrefix = '@')
        {
            foreach(MultiDbTable table in Tables)
            {
                string filename = $"{table.ClassName}.xml";
                string file = Path.Combine(output, filename);

                using(FileStream fs = new FileStream(file, FileMode.Create, FileAccess.Write))
                {
                    using(StreamWriter sw = new StreamWriter(fs, Encoding.UTF8))
                    {
                        string code = GenerateQuery(table, config, parameterPrefix);
                        sw.Write(code);
                    }
                }
            }
        }

        private string GenerateClass(MultiDbTable table, MultiDbGeneratorConfig config)
        {
            StringBuilder sb = new StringBuilder();

            // Usings
            sb.AppendLine($"using Byte.Toolkit.Db;");
            sb.AppendLine($"using Byte.Toolkit.Db.Attributes;");
            sb.AppendLine($"using Byte.Toolkit.Db.Converters;");
            sb.AppendLine($"using Byte.Toolkit.Db.Databases;");

            if (config.PropertyType == PropertyType.GetSetWithFields)
                sb.AppendLine($"{config.ParentClassUsing}");

            sb.AppendLine($"using System;");
            sb.AppendLine();

            // Namespace
            sb.AppendLine($"namespace {config.ClassesNamespace}");
            sb.AppendLine($"{{");

            // Class
            sb.AppendLine($@"    [Table(""{table.TableName}"")]");
            if (config.PropertyType == PropertyType.GetSet)
                sb.AppendLine($"    public class {table.ClassName}");
            else
                sb.AppendLine($"    public class {table.ClassName} : {config.ParentClass}");
            
            sb.AppendLine($"    {{");

            // Properties
            int i = 0;
            foreach (MultiDbColumn col in table.Columns)
            {
                // Type
                Type propertyType = col.PropertyType;
                string strType = GetSimplifiedType(propertyType);

                if (col.IsPropertyTypeNullable || col.PropertyType == typeof(string))
                    strType += "?";

                // Field
                string fieldname = Utils.GetFieldname(col.PropertyName);
                if (config.PropertyType == PropertyType.GetSetWithFields)
                    sb.AppendLine($"        private {strType} {fieldname};");

                // Attributes
                sb.AppendLine($@"        [Column(""{col.ColumnName}"")]");

                // Converters
                foreach(IDatabase database in Databases)
                {
                    Type columnType = database.PropertyToColumnMapping[propertyType];

                    if (columnType != propertyType)
                    {
                        IConverter converter = database.Converters.First(x => 
                                                                         x.PropertyType == propertyType &&
                                                                         x.ColumnType == columnType);
                        sb.AppendLine($"        [Converter(typeof({database.GetType().Name}), typeof({converter.GetType().Name}))]");
                    }
                }

                // Property
                if (config.PropertyType == PropertyType.GetSet)
                    sb.AppendLine($"        public {strType} {col.PropertyName} {{ get; set; }}");
                else
                {
                    sb.AppendLine($"        public {strType} {col.PropertyName}");
                    sb.AppendLine($"        {{");
                    sb.AppendLine($"            get => {fieldname};");
                    sb.AppendLine($"            {string.Format(config.PropertySetTemplate, fieldname, col.PropertyName)}");
                    sb.AppendLine($"        }}");
                }

                if (i++ < table.Columns.Count - 1)
                    sb.AppendLine();
            }
            

            sb.AppendLine($"    }}");

            sb.AppendLine($"}}");

            return sb.ToString();
        }

        private string GenerateDatabaseLayer(MultiDbGeneratorConfig config)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine($"using Byte.Toolkit.Db;");
            sb.AppendLine($"using Byte.Toolkit.Db.Databases;");
            sb.AppendLine($"using {config.ClassesNamespace};");
            sb.AppendLine($"using System.Data.Common;");
            sb.AppendLine($"using System.Reflection;");
            sb.AppendLine($"using System.IO;");
            sb.AppendLine();

            // Namespace
            sb.AppendLine($"namespace {config.LayersNamespace}");
            sb.AppendLine($"{{");

            // Main layer
            sb.AppendLine($"    public class {config.LayerClassName}");
            sb.AppendLine($"    {{");

            // Static constructor
            sb.AppendLine($"        public static void Init(string queriesPath)");
            sb.AppendLine($"        {{");
            sb.AppendLine($"            MultiDbManager.RegisterDbObjectTypes(Assembly.GetAssembly(typeof({Tables[0].ClassName})));");
            sb.AppendLine();

            foreach (MultiDbTable table in Tables)
                sb.AppendLine(@$"            MultiDbManager.AddQueriesFile(typeof({table.ClassName}), Path.Combine(queriesPath, ""{table.ClassName}.xml""));");

            sb.AppendLine($"        }}");
            sb.AppendLine();

            // Constructor
            sb.AppendLine($"        public {config.LayerClassName}()");
            sb.AppendLine($"        {{");
            sb.AppendLine(@$"            DbProviderFactories.RegisterFactory(""{config.DefaultProviderName}"", ""{config.DefaultFactoryType}"");");
            sb.AppendLine(@$"            DbManager = new MultiDbManager<{config.DefaultDatabaseType}>(""{config.DefaultConnectionString}"", ""{config.DefaultProviderName}"");");
            sb.AppendLine();

            foreach (MultiDbTable table in Tables)
                sb.AppendLine($"            {table.ClassName} = new {table.ClassName}Layer(DbManager);");

            sb.AppendLine($"        }}");
            sb.AppendLine();

            // Properties
            sb.AppendLine($"        public MultiDbManager DbManager {{ get; set; }}");

            foreach(MultiDbTable table in Tables)
            {
                sb.AppendLine($"        public {table.ClassName}Layer {table.ClassName} {{ get; set; }}");
            }

            sb.AppendLine($"    }}");

            sb.AppendLine($"}}");

            return sb.ToString();
        }

        private string GenerateClassLayer(MultiDbTable table, MultiDbGeneratorConfig config)
        {
            StringBuilder sb = new StringBuilder();

            MultiDbColumn idCol = table.Columns[0];
            string idParameterType = GetSimplifiedType(idCol.PropertyType);
            string idParameterName = Utils.GetParameterName(idCol.PropertyName);
            string instanceName = Utils.GetInstanceName(table.ClassName);

            sb.AppendLine($"using Byte.Toolkit.Db;");
            sb.AppendLine($"using {config.ClassesNamespace};");
            sb.AppendLine($"using System.Collections.Generic;");
            sb.AppendLine($"using System.Data.Common;");
            sb.AppendLine();

            // Namespace
            sb.AppendLine($"namespace {config.LayersNamespace}");
            sb.AppendLine($"{{");

            // Class
            sb.AppendLine($"    public class {table.ClassName}Layer : MultiDbObjectLayer<{table.ClassName}>");
            sb.AppendLine($"    {{");

            // Constructor
            sb.AppendLine($"        public {table.ClassName}Layer(MultiDbManager db)");
            sb.AppendLine($"            : base(db) {{ }}");
            sb.AppendLine();

            // Select all
            sb.AppendLine(@$"        public List<{table.ClassName}> SelectAll{table.ClassName}s() => DbManager.FillObjects<{table.ClassName}>(Queries[""SelectAll{table.ClassName}s""]);");
            sb.AppendLine();

            // Select by id
            sb.AppendLine($"        public {table.ClassName}? Select{table.ClassName}ById({idParameterType} {idParameterName})");
            sb.AppendLine($"        {{");
            sb.AppendLine($"            List<DbParameter> parameters = new List<DbParameter>();");
            sb.AppendLine(@$"            parameters.Add(DbManager.CreateParameter(""{idParameterName}"", {idParameterName}));");
            sb.AppendLine(@$"            return DbManager.FillSingleObject<{table.ClassName}>(Queries[""Select{table.ClassName}ById""], parameters: parameters);");
            sb.AppendLine($"        }}");
            sb.AppendLine();

            // Insert
            sb.AppendLine($"        public int Insert{table.ClassName}({table.ClassName} {instanceName})");
            sb.AppendLine($"        {{");
            sb.AppendLine($"            List<DbParameter> parameters = new List<DbParameter>();");

            foreach (MultiDbColumn col in table.Columns)
            {
                string parameterName = Utils.GetParameterName(col.PropertyName);
                sb.AppendLine(@$"            parameters.Add(DbManager.CreateParameter(""{parameterName}"", {instanceName}.{col.PropertyName}));");
            }

            sb.AppendLine(@$"            return DbManager.ExecuteNonQuery(Queries[""Insert{table.ClassName}""], parameters: parameters);");
            sb.AppendLine($"        }}");
            sb.AppendLine();

            // Update
            sb.AppendLine($"        public int Update{table.ClassName}({table.ClassName} {instanceName})");
            sb.AppendLine($"        {{");
            sb.AppendLine($"            List<DbParameter> parameters = new List<DbParameter>();");

            foreach (MultiDbColumn col in table.Columns)
            {
                string parameterName = Utils.GetParameterName(col.PropertyName);
                sb.AppendLine(@$"            parameters.Add(DbManager.CreateParameter(""{parameterName}"", {instanceName}.{col.PropertyName}));");
            }

            sb.AppendLine(@$"            return DbManager.ExecuteNonQuery(Queries[""Update{table.ClassName}""], parameters: parameters);");
            sb.AppendLine($"        }}");
            sb.AppendLine();

            // Delete
            sb.AppendLine($"        public int Delete{table.ClassName}ById({idParameterType} {idParameterName})");
            sb.AppendLine($"        {{");
            sb.AppendLine($"            List<DbParameter> parameters = new List<DbParameter>();");
            sb.AppendLine(@$"            parameters.Add(DbManager.CreateParameter(""{idParameterName}"", {idParameterName}));");
            sb.AppendLine(@$"            return DbManager.ExecuteNonQuery(Queries[""Delete{table.ClassName}ById""], parameters: parameters);");
            sb.AppendLine($"        }}");

            sb.AppendLine($"    }}");
            sb.AppendLine($"}}");

            return sb.ToString();
        }

        private string GenerateQuery(MultiDbTable table, MultiDbGeneratorConfig config, char parameterPrefix)
        {
            StringBuilder sb = new StringBuilder();

            List<string> columns = new List<string>();
            List<string> parameters = new List<string>();

            foreach(MultiDbColumn col in table.Columns)
            {
                columns.Add(col.ColumnName);
                parameters.Add(parameterPrefix + Utils.GetParameterName(col.PropertyName));
            }

            sb.AppendLine($@"<?xml version=""1.0"" encoding=""utf-8"" ?>");
            sb.AppendLine($"<Queries>");

            // SelectAll
            sb.AppendLine($@"  <Query Name=""SelectAll{table.ClassName}s"">");
            sb.AppendLine($"    SELECT");
            sb.AppendLine($"      {string.Join(", ", columns)}");
            sb.AppendLine($"    FROM");
            sb.AppendLine($"      {table.TableName}");
            sb.AppendLine($"  </Query>");

            // SelectById
            sb.AppendLine($@"  <Query Name=""Select{table.ClassName}ById"">");
            sb.AppendLine($"    SELECT");
            sb.AppendLine($"      {string.Join(", ", columns)}");
            sb.AppendLine($"    FROM");
            sb.AppendLine($"      {table.TableName}");
            sb.AppendLine($"    WHERE");
            sb.AppendLine($"      {columns[0]} = {parameters[0]}");
            sb.AppendLine($"  </Query>");

            // Insert
            sb.AppendLine($@"  <Query Name=""Insert{table.ClassName}"">");
            sb.AppendLine($"    INSERT INTO {table.TableName} ({string.Join(", ", columns)})");
            sb.AppendLine($"    VALUES ({string.Join(", ", parameters)})");
            sb.AppendLine($"  </Query>");

            // Update
            sb.AppendLine($@"  <Query Name=""Update{table.ClassName}"">");
            sb.AppendLine($"    UPDATE {table.TableName}");
            sb.AppendLine($"    SET");

            if (columns.Count > 1)
            {
                for (int i = 1; i < columns.Count; i++)
                {
                    sb.Append($"      {columns[i]} = {parameters[i]}");

                    if (i < columns.Count - 1)
                        sb.AppendLine(", ");
                }
                sb.AppendLine();
            }
            else
                sb.AppendLine($"      {columns[0]} = {parameters[0]}");

            sb.AppendLine($"    WHERE");
            sb.AppendLine($"      {columns[0]} = {parameters[0]}");
            sb.AppendLine($"  </Query>");

            // Delete
            sb.AppendLine($@"  <Query Name=""Delete{table.ClassName}ById"">");
            sb.AppendLine($"    DELETE FROM {table.TableName}");
            sb.AppendLine($"    WHERE");
            sb.AppendLine($"      {columns[0]} = {parameters[0]}");
            sb.AppendLine($"  </Query>");
            sb.AppendLine($"</Queries>");

            return sb.ToString();
        }

        private string GetSimplifiedType(Type t)
        {
            if (SimplifiedTypes.ContainsKey(t))
                return SimplifiedTypes[t];

            return t.Name;
        }
    }
}
