﻿using System;

namespace Byte.Toolkit.Db.Converters
{
    /// <summary>
    /// TimeSpan to String converter
    /// </summary>
    public class TimeSpanToStringConverter : IConverter
    {
        /// <inheritdoc/>
        public Type PropertyType => typeof(TimeSpan);

        /// <inheritdoc/>
        public Type ColumnType => typeof(string);

        /// <inheritdoc/>
        public object ConvertToColumn(object? obj)
        {
            if (obj == null)
                return DBNull.Value;

            if (obj is TimeSpan ts)
                return ts.ToString("c");

            throw new InvalidTypeException(obj.GetType(), PropertyType);
        }

        /// <inheritdoc/>
        public object ConvertToProperty(object obj)
        {
            if (obj is string s)
                return TimeSpan.Parse(s);

            throw new InvalidTypeException(obj.GetType(), ColumnType);
        }
    }
}
