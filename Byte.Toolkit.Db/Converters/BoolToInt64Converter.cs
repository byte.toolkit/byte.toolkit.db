﻿using System;

namespace Byte.Toolkit.Db.Converters
{
    /// <summary>
    /// Bool to Int64 converter
    /// </summary>
    public class BoolToInt64Converter : IConverter
    {
        /// <inheritdoc/>
        public Type PropertyType => typeof(bool);

        /// <inheritdoc/>
        public Type ColumnType => typeof(long);

        /// <inheritdoc/>
        public object ConvertToColumn(object? obj)
        {
            if (obj == null)
                return DBNull.Value;

            if (obj is bool b)
                return b ? 1 : 0;

            throw new InvalidTypeException(obj.GetType(), PropertyType);
        }

        /// <inheritdoc/>
        public object ConvertToProperty(object obj)
        {
            if (obj is long l)
                return l > 0 ? true : false;

            throw new InvalidTypeException(obj.GetType(), ColumnType);
        }
    }
}
