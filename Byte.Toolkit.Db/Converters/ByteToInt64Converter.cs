﻿using System;

namespace Byte.Toolkit.Db.Converters
{
    /// <summary>
    /// Byte to Int64 converter
    /// </summary>
    public class ByteToInt64Converter : IConverter
    {
        /// <inheritdoc/>
        public Type PropertyType => typeof(byte);

        /// <inheritdoc/>
        public Type ColumnType => typeof(long);

        /// <inheritdoc/>
        public object ConvertToColumn(object? obj)
        {
            if (obj == null)
                return DBNull.Value;

            if (obj is byte b)
                return (long)b;

            throw new InvalidTypeException(obj.GetType(), PropertyType);
        }

        /// <inheritdoc/>
        public object ConvertToProperty(object obj)
        {
            if (obj is long l)
                return (byte)l;

            throw new InvalidTypeException(obj.GetType(), ColumnType);
        }
    }
}
