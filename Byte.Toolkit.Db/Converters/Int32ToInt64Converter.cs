﻿using System;

namespace Byte.Toolkit.Db.Converters
{
    /// <summary>
    /// Int32 to Int64 converter
    /// </summary>
    public class Int32ToInt64Converter : IConverter
    {
        /// <inheritdoc/>
        public Type PropertyType => typeof(int);

        /// <inheritdoc/>
        public Type ColumnType => typeof(long);

        /// <inheritdoc/>
        public object ConvertToColumn(object? obj)
        {
            if (obj == null)
                return DBNull.Value;

            if (obj is int i)
                return (long)i;

            throw new InvalidTypeException(obj.GetType(), PropertyType);
        }

        /// <inheritdoc/>
        public object ConvertToProperty(object obj)
        {
            if (obj is long l)
                return (int)l;

            throw new InvalidTypeException(obj.GetType(), ColumnType);
        }
    }
}
