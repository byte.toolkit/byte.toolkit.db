﻿using System;

namespace Byte.Toolkit.Db.Converters
{
    /// <summary>
    /// Interface specifying types and methods for conversion between
    /// properties types and columns types
    /// </summary>
    public interface IConverter
    {
        /// <summary>
        /// Property type
        /// </summary>
        Type PropertyType { get; }

        /// <summary>
        /// Column type
        /// </summary>
        Type ColumnType { get; }

        /// <summary>
        /// Convert a property value to a column value.
        /// Returns <see cref="DBNull.Value"/> if obj is null
        /// </summary>
        /// <param name="obj">Property value</param>
        /// <returns>Column value</returns>
        object ConvertToColumn(object? obj);

        /// <summary>
        /// Convert a column value to a property value
        /// </summary>
        /// <param name="obj">Column value</param>
        /// <returns>Property value</returns>
        object ConvertToProperty(object obj);
    }
}
