﻿using System;

namespace Byte.Toolkit.Db.Converters
{
    /// <summary>
    /// Char to String converter
    /// </summary>
    public class CharToStringConverter : IConverter
    {
        /// <inheritdoc/>
        public Type PropertyType => typeof(char);

        /// <inheritdoc/>
        public Type ColumnType => typeof(string);

        /// <inheritdoc/>
        public object ConvertToColumn(object? obj)
        {
            if (obj == null)
                return DBNull.Value;

            if (obj is char c)
                return c.ToString();

            throw new InvalidTypeException(obj.GetType(), PropertyType);
        }

        /// <inheritdoc/>
        public object ConvertToProperty(object obj)
        {
            if (obj is string s)
                return s[0];

            throw new InvalidTypeException(obj.GetType(), ColumnType);
        }
    }
}
