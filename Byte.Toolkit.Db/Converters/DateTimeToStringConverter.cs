﻿using System;

namespace Byte.Toolkit.Db.Converters
{
    /// <summary>
    /// DateTime to String converter
    /// </summary>
    public class DateTimeToStringConverter : IConverter
    {
        /// <inheritdoc/>
        public Type PropertyType => typeof(DateTime);

        /// <inheritdoc/>
        public Type ColumnType => typeof(string);

        /// <inheritdoc/>
        public object ConvertToColumn(object? obj)
        {
            if (obj == null)
                return DBNull.Value;

            if (obj is DateTime dt)
                return dt.ToString("yyyy-MM-dd HH:mm:ss.FFFFFFF");

            throw new InvalidTypeException(obj.GetType(), PropertyType);
        }

        /// <inheritdoc/>
        public object ConvertToProperty(object obj)
        {
            if (obj is string s)
                return DateTime.Parse(s);

            throw new InvalidTypeException(obj.GetType(), ColumnType);
        }
    }
}
