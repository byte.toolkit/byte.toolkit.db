﻿using System;

namespace Byte.Toolkit.Db.Converters
{
    /// <summary>
    /// No conversion converter
    /// </summary>
    public class NoConversionConverter : IConverter
    {
        /// <inheritdoc/>
        public Type PropertyType => typeof(object);

        /// <inheritdoc/>
        public Type ColumnType => typeof(object);

        /// <inheritdoc/>
        public object ConvertToColumn(object? obj) => obj == null ? DBNull.Value : obj;

        /// <inheritdoc/>
        public object ConvertToProperty(object obj) => obj;
    }
}
