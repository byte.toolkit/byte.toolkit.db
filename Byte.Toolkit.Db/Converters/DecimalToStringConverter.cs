﻿using System;

namespace Byte.Toolkit.Db.Converters
{
    /// <summary>
    /// Decimal to String converter
    /// </summary>
    public class DecimalToStringConverter : IConverter
    {
        /// <inheritdoc/>
        public Type PropertyType => typeof(decimal);

        /// <inheritdoc/>
        public Type ColumnType => typeof(string);

        /// <inheritdoc/>
        public object ConvertToColumn(object? obj)
        {
            if (obj == null)
                return DBNull.Value;

            if (obj is decimal d)
                return d.ToString("0.0###########################");

            throw new InvalidTypeException(obj.GetType(), PropertyType);
        }

        /// <inheritdoc/>
        public object ConvertToProperty(object obj)
        {
            if (obj is string s)
                return decimal.Parse(s);

            throw new InvalidTypeException(obj.GetType(), ColumnType);
        }
    }
}
