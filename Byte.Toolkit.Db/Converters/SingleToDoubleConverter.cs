﻿using System;

namespace Byte.Toolkit.Db.Converters
{
    /// <summary>
    /// Single to Double converter
    /// </summary>
    public class SingleToDoubleConverter : IConverter
    {
        /// <inheritdoc/>
        public Type PropertyType => typeof(float);

        /// <inheritdoc/>
        public Type ColumnType => typeof(double);

        /// <inheritdoc/>
        public object ConvertToColumn(object? obj)
        {
            if (obj == null)
                return DBNull.Value;

            if (obj is float f)
                return (double)f;

            throw new InvalidTypeException(obj.GetType(), PropertyType);
        }

        /// <inheritdoc/>
        public object ConvertToProperty(object obj)
        {
            if (obj is double d)
                return (float)d;

            throw new InvalidTypeException(obj.GetType(), ColumnType);
        }
    }
}
