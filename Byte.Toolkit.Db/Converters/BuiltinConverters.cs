﻿using System.Collections.Generic;

namespace Byte.Toolkit.Db.Converters
{
    /// <summary>
    /// Builtin converters
    /// </summary>
    public static class BuiltinConverters
    {
        /// <summary>
        /// Builtin converters
        /// </summary>
        public static readonly IEnumerable<IConverter> Converters =
            new IConverter[]
            {
                new BoolToInt64Converter(),
                new ByteToInt64Converter(),
                new Int16ToInt64Converter(),
                new Int32ToInt64Converter(),
                new SingleToDoubleConverter(),
                new DecimalToStringConverter(),
                new CharToStringConverter(),
                new DateTimeToStringConverter(),
                new TimeSpanToStringConverter()
            };

        /// <summary>
        /// Converter that returns the object as is
        /// </summary>
        public static IConverter NoConversionConverter = new NoConversionConverter();
    }
}
