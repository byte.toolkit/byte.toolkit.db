﻿using System;

namespace Byte.Toolkit.Db.Converters
{
    /// <summary>
    /// Int16 to Int64 converter
    /// </summary>
    public class Int16ToInt64Converter : IConverter
    {
        /// <inheritdoc/>
        public Type PropertyType => typeof(short);

        /// <inheritdoc/>
        public Type ColumnType => typeof(long);

        /// <inheritdoc/>
        public object ConvertToColumn(object? obj)
        {
            if (obj == null)
                return DBNull.Value;

            if (obj is short s)
                return (long)s;

            throw new InvalidTypeException(obj.GetType(), PropertyType);
        }

        /// <inheritdoc/>
        public object ConvertToProperty(object obj)
        {
            if (obj is long l)
                return (short)l;

            throw new InvalidTypeException(obj.GetType(), ColumnType);
        }
    }
}
