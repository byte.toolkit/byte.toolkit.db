﻿using System.Collections.Generic;

namespace Byte.Toolkit.Db
{
    /// <summary>
    /// DbObject layer abstract class
    /// </summary>
    /// <typeparam name="T">DbObject type</typeparam>
    public class MultiDbObjectLayer<T>
    {
        /// <summary>
        /// Constructor for <see cref="MultiDbObjectLayer{T}"/>
        /// </summary>
        /// <param name="db"><see cref="MultiDbManager"/> instance</param>
        public MultiDbObjectLayer(MultiDbManager db)
        {
            DbManager = db;
        }

        /// <summary>
        /// Database management class
        /// </summary>
        public MultiDbManager DbManager { get; }

        /// <summary>
        /// Queries
        /// </summary>
        public Dictionary<string, string> Queries => MultiDbManager.Queries[typeof(T)];
    }
}
