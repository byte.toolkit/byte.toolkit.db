﻿using System;

namespace Byte.Toolkit.Db
{
    /// <summary>
    /// Missing table attribute exception
    /// </summary>
    public class MissingTableAttributeException : Exception
    {
        /// <summary>
        /// Constructor for <see cref="MissingTableAttributeException"/>
        /// </summary>
        /// <param name="t">Object type</param>
        public MissingTableAttributeException(Type t) : base($"TableAttribute is missing on type '{t}'") { }
    }

    /// <summary>
    /// Database already registered exception
    /// </summary>
    public class DatabaseAlreadyRegisteredException : Exception
    {
        /// <summary>
        /// Constructor for <see cref="DatabaseAlreadyRegisteredException"/>
        /// </summary>
        /// <param name="t">Object type</param>
        public DatabaseAlreadyRegisteredException(Type t) : base($"Database '{t}' already registered") { }
    }

    /// <summary>
    /// DbObject already registered exception
    /// </summary>
    public class DbObjectAlreadyRegisteredException : Exception
    {
        /// <summary>
        /// Constructor for <see cref="DbObjectAlreadyRegisteredException"/>
        /// </summary>
        /// <param name="t">Object type</param>
        public DbObjectAlreadyRegisteredException(Type t) : base($"Type '{t}' alredy registered") { }
    }

    /// <summary>
    /// Object not registered exception
    /// </summary>
    public class ObjectNotRegisteredException : Exception
    {
        /// <summary>
        /// Constructor for <see cref="ObjectNotRegisteredException"/>
        /// </summary>
        /// <param name="t"></param>
        public ObjectNotRegisteredException(Type t) : base($"Type '{t}' not registered") { }
    }

    /// <summary>
    /// Invalid queries file exception
    /// </summary>
    public class InvalidQueriesFileException : Exception
    {
        /// <summary>
        /// Constructor for <see cref="InvalidQueriesFileException"/>
        /// </summary>
        /// <param name="message">Exception message</param>
        public InvalidQueriesFileException(string message) : base(message) { }
    }

    /// <summary>
    /// Invalid type exception
    /// </summary>
    public class InvalidTypeException : Exception
    {
        /// <summary>
        /// Constructor for <see cref="InvalidTypeException"/>
        /// </summary>
        /// <param name="t">Type encountered</param>
        /// <param name="expectedType">Expected type</param>
        public InvalidTypeException(Type t, Type expectedType) : base($"Invalid type '{t}'. Expected type: {expectedType}") { }
    }
}
