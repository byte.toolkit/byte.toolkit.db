﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Reflection;
using System.Xml;
using Byte.Toolkit.Db.Attributes;
using FastMember;

namespace Byte.Toolkit.Db
{
    /// <summary>
    /// Database management class.
    /// Allows objects selection with high speed reflection
    /// and queries management
    /// </summary>
    public class SingleDbManager : DbManager
    {
        #region Constructors

        static SingleDbManager()
        {
            TypeAccessors = new Dictionary<Type, TypeAccessor>();
            ColumnsPropertiesNamesMapping = new Dictionary<Type, Dictionary<string, string>>();
            Queries = new Dictionary<Type, Dictionary<string, string>>();
        }

#if NETSTANDARD2_0
        /// <summary>
        /// Constructor for <see cref="SingleDbManager"/>
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <param name="factory">Database provider factory</param>
        public SingleDbManager(string connectionString, DbProviderFactory factory)
            : base(connectionString, factory)
        { }
#elif (NETSTANDARD2_1_OR_GREATER || NET461_OR_GREATER)
        /// <summary>
        /// Constructor for <see cref="SingleDbManager"/>
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <param name="provider">Database provider</param>
        public SingleDbManager(string connectionString, string provider)
            : base(connectionString, provider)
        { }
#endif

        #endregion

        #region Properties

        /// <summary>
        /// Type accessors
        /// </summary>
        public static Dictionary<Type, TypeAccessor> TypeAccessors { get; set; }

        /// <summary>
        /// Type column-property mappings
        /// </summary>
        public static Dictionary<Type, Dictionary<string, string>> ColumnsPropertiesNamesMapping { get; set; }

        /// <summary>
        /// Queries
        /// </summary>
        public static Dictionary<Type, Dictionary<string, string>> Queries { get; private set; }

        #endregion

        #region Static methods

        /// <summary>
        /// Register a DbObject
        /// </summary>
        /// <param name="objectType">Object type</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="MissingTableAttributeException"></exception>
        /// <exception cref="DbObjectAlreadyRegisteredException"></exception>
        public static void RegisterDbObjectType(Type objectType)
        {
            if (objectType == null)
                throw new ArgumentNullException(nameof(objectType));
            if (objectType.GetCustomAttribute<TableAttribute>() == null)
                throw new MissingTableAttributeException(objectType);
            if (TypeAccessors.ContainsKey(objectType) || ColumnsPropertiesNamesMapping.ContainsKey(objectType))
                throw new DbObjectAlreadyRegisteredException(objectType);

            TypeAccessors.Add(objectType, TypeAccessor.Create(objectType));
            ColumnsPropertiesNamesMapping.Add(objectType, new Dictionary<string, string>());
            
            foreach(PropertyInfo prop in objectType.GetProperties())
            {
                ColumnAttribute? attr = prop.GetCustomAttribute<ColumnAttribute>();

                if (attr != null)
                {
                    string columnName = attr.Name ?? prop.Name;
                    ColumnsPropertiesNamesMapping[objectType].Add(columnName, prop.Name);
                }
            }
        }

        /// <summary>
        /// Register database object types from assemblies
        /// </summary>
        /// <param name="assemblies">Assemblies to scan</param>
        public static void RegisterDbObjectTypes(params Assembly[] assemblies)
        {
            foreach(Assembly assembly in assemblies)
            {
                Type[] types = assembly.GetTypes();
                
                foreach(Type type in types)
                {
                    TableAttribute? attr = type.GetCustomAttribute<TableAttribute>();

                    if (attr != null)
                        RegisterDbObjectType(type);
                }
            }
        }

        /// <summary>
        /// Add queries for a DbObject
        /// </summary>
        /// <param name="objectType">DbObject type</param>
        /// <param name="queries">Queries</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ObjectNotRegisteredException"></exception>
        public static void AddQueries(Type objectType, Dictionary<string, string> queries)
        {
            if (objectType == null)
                throw new ArgumentNullException(nameof(objectType));
            if (queries == null)
                throw new ArgumentNullException(nameof(queries));
            if (!TypeAccessors.ContainsKey(objectType))
                throw new ObjectNotRegisteredException(objectType);

            if (!Queries.ContainsKey(objectType))
                Queries.Add(objectType, queries);
            else
                Queries[objectType] = queries;
        }

        /// <summary>
        /// Add queries file for a DbObject
        /// </summary>
        /// <param name="objectType">DbObject type</param>
        /// <param name="file">Query file</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ObjectNotRegisteredException"></exception>
        /// <exception cref="InvalidQueriesFileException"></exception>
        public static void AddQueriesFile(Type objectType, string file)
        {
            if (objectType == null)
                throw new ArgumentNullException(nameof(objectType));
            if (file == null)
                throw new ArgumentNullException(nameof(file));
            if (!TypeAccessors.ContainsKey(objectType))
                throw new ObjectNotRegisteredException(objectType);

            XmlDocument doc = new XmlDocument();
            doc.Load(file);

            Queries.Add(objectType, new Dictionary<string, string>());

            XmlNodeList requestList = doc.SelectNodes("/Queries/Query");

            foreach (XmlNode queryNode in requestList)
            {
                XmlAttribute nameAttr = queryNode.Attributes["Name"];

                if (nameAttr == null)
                    throw new InvalidQueriesFileException("Name attribute missing on a Query node");

                Queries[objectType].Add(nameAttr.Value, queryNode.InnerText);
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Fill a single object with a query
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <param name="commandText">Command text</param>
        /// <param name="commandType">Command type</param>
        /// <param name="parameters">Command parameters</param>
        /// <returns>DbObject</returns>
        /// <exception cref="ObjectDisposedException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ObjectNotRegisteredException"></exception>
        public virtual T? FillSingleObject<T>(string commandText, CommandType commandType = CommandType.Text, IEnumerable<DbParameter>? parameters = null)
            where T : class
        {
            if (_disposed)
                throw new ObjectDisposedException(typeof(DbManager).FullName);
            if (commandText == null)
                throw new ArgumentNullException(nameof(commandText));

            Type objectType = typeof(T);

            if (!TypeAccessors.ContainsKey(objectType))
                throw new ObjectNotRegisteredException(objectType);

            T obj = Activator.CreateInstance<T>();
            TypeAccessor ta = TypeAccessors[objectType];

            using (DbCommand command = Connection.CreateCommand())
            {
                command.CommandType = commandType;
                command.CommandText = commandText;

                if (Transaction != null)
                    command.Transaction = Transaction;

                if (parameters != null)
                {
                    foreach (DbParameter param in parameters)
                        command.Parameters.Add(param);
                }

                using (DbDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            string fieldName = reader.GetName(i);

                            if (ColumnsPropertiesNamesMapping[objectType].ContainsKey(fieldName))
                            {
                                object readerValue = reader[fieldName];
                                if (!(readerValue is DBNull))
                                    ta[obj, ColumnsPropertiesNamesMapping[objectType][fieldName]] = readerValue;
                            }
                        }
                    }
                    else
                    {
                        return default(T);
                    }
                    reader.Close();
                }
            }

            return obj;
        }

        /// <summary>
        /// Fill an object list with a query
        /// </summary>
        /// <typeparam name="T">DbObject type</typeparam>
        /// <param name="commandText">Command text</param>
        /// <param name="commandType">Command type</param>
        /// <param name="parameters">Command parameters</param>
        /// <returns>DbObject list</returns>
        /// <exception cref="ObjectDisposedException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ObjectNotRegisteredException"></exception>
        public virtual List<T> FillObjects<T>(string commandText, CommandType commandType = CommandType.Text, IEnumerable<DbParameter>? parameters = null)
            where T : class
        {
            if (_disposed)
                throw new ObjectDisposedException(typeof(DbManager).FullName);
            if (commandText == null)
                throw new ArgumentNullException(nameof(commandText));

            Type objectType = typeof(T);

            if (!TypeAccessors.ContainsKey(objectType))
                throw new ObjectNotRegisteredException(objectType);

            List<T> list = new List<T>();
            TypeAccessor ta = TypeAccessors[objectType];

            using (DbCommand command = Connection.CreateCommand())
            {
                command.CommandType = commandType;
                command.CommandText = commandText;

                if (Transaction != null)
                    command.Transaction = Transaction;

                if (parameters != null)
                {
                    foreach (DbParameter param in parameters)
                        command.Parameters.Add(param);
                }

                using (DbDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        T obj = Activator.CreateInstance<T>();

                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            string fieldName = reader.GetName(i);

                            if (ColumnsPropertiesNamesMapping[objectType].ContainsKey(fieldName))
                            {
                                object readerValue = reader[fieldName];
                                if (!(readerValue is DBNull))
                                    ta[obj, ColumnsPropertiesNamesMapping[objectType][fieldName]] = readerValue;
                            }
                        }

                        list.Add(obj);
                    }

                    reader.Close();
                }
            }

            return list;
        }

        #endregion
    }
}
