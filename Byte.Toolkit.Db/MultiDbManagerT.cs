﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using Byte.Toolkit.Db.Converters;
using Byte.Toolkit.Db.Databases;
using FastMember;

namespace Byte.Toolkit.Db
{
    /// <summary>
    /// Database management class.
    /// Allows single objects to work with multiple databases
    /// </summary>
    /// <typeparam name="TDatabase">Database type</typeparam>
    public class MultiDbManager<TDatabase> : MultiDbManager
        where TDatabase : IDatabase
    {
        #region Constructors

#if NETSTANDARD2_0
        /// <summary>
        /// Constructor for <see cref="MultiDbManager{TDatabase}"/>
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <param name="factory">Database provider factory</param>
        public MultiDbManager(string connectionString, DbProviderFactory factory)
            : base(connectionString, factory)
        {
            DatabaseType = typeof(TDatabase);
            Database = (IDatabase)Activator.CreateInstance(DatabaseType);
        }
#elif (NETSTANDARD2_1_OR_GREATER || NET461_OR_GREATER)
        /// <summary>
        /// Constructor for <see cref="MultiDbManager{TDatabase}"/>
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <param name="provider">Database provider</param>
        public MultiDbManager(string connectionString, string provider)
            : base(connectionString, provider)
        {
            DatabaseType = typeof(TDatabase);
            Database = (IDatabase)Activator.CreateInstance(DatabaseType);
        }
#endif

        #endregion

        #region Properties

        private Type DatabaseType { get; }
        private IDatabase Database { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Create and return a new parameter using the database converters for the value
        /// </summary>
        /// <param name="name">Parameter name</param>
        /// <param name="value">Parameter value</param>
        /// <param name="paramDirection">Parameter direction</param>
        /// <returns>New DbParameter</returns>
        /// <exception cref="ObjectDisposedException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public override DbParameter CreateParameter(string name, object? value, ParameterDirection paramDirection = ParameterDirection.Input)
        {
            if (_disposed)
                throw new ObjectDisposedException(typeof(DbManager).FullName);
            if (name == null)
                throw new ArgumentNullException(nameof(name));

            DbParameter param = Factory.CreateParameter();
            param.ParameterName = name;

            if (value != null)
            {
                Type valueType = value.GetType();
                valueType = Nullable.GetUnderlyingType(valueType) ?? valueType;

                IConverter? converter = Database.Converters.FirstOrDefault(x =>
                                                                           x.PropertyType == valueType &&
                                                                           x.ColumnType == Database.PropertyToColumnMapping[valueType]);
                if (converter != null)
                    param.Value = converter.ConvertToColumn(value);
                else
                    param.Value = value;
            }
            else
                param.Value = DBNull.Value;


            param.Direction = paramDirection;
            return param;
        }

        /// <inheritdoc/>
        public override T? FillSingleObject<T>(string commandText, CommandType commandType = CommandType.Text, IEnumerable<DbParameter>? parameters = null)
            where T : class
        {
            if (_disposed)
                throw new ObjectDisposedException(typeof(DbManager).FullName);
            if (commandText == null)
                throw new ArgumentNullException(nameof(commandText));

            Type objectType = typeof(T);

            if (!TypeAccessors.ContainsKey(objectType))
                throw new ObjectNotRegisteredException(objectType);

            T obj = Activator.CreateInstance<T>();
            TypeAccessor ta = TypeAccessors[objectType];

            using (DbCommand command = Connection.CreateCommand())
            {
                command.CommandType = commandType;
                command.CommandText = commandText;

                if (Transaction != null)
                    command.Transaction = Transaction;

                if (parameters != null)
                {
                    foreach (DbParameter param in parameters)
                        command.Parameters.Add(param);
                }

                using (DbDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            string fieldName = reader.GetName(i);

                            if (ColumnsPropertiesNamesMapping[objectType].ContainsKey(fieldName))
                            {
                                string propertyName = ColumnsPropertiesNamesMapping[objectType][fieldName];

                                Func<object, object> func = PropertiesConverters[objectType][propertyName][DatabaseType].ConvertToProperty;

                                object readerValue = reader[fieldName];
                                if (!(readerValue is DBNull))
                                    ta[obj, propertyName] = func(readerValue);
                            }
                        }
                    }
                    else
                    {
                        return default(T);
                    }
                    reader.Close();
                }
            }

            return obj;
        }

        /// <inheritdoc/>
        public override List<T> FillObjects<T>(string commandText, CommandType commandType = CommandType.Text, IEnumerable<DbParameter>? parameters = null)
            where T: class
        {
            if (_disposed)
                throw new ObjectDisposedException(typeof(DbManager).FullName);
            if (commandText == null)
                throw new ArgumentNullException(nameof(commandText));

            Type objectType = typeof(T);

            if (!TypeAccessors.ContainsKey(objectType))
                throw new ObjectNotRegisteredException(objectType);

            List<T> list = new List<T>();
            TypeAccessor ta = TypeAccessors[objectType];

            using (DbCommand command = Connection.CreateCommand())
            {
                command.CommandType = commandType;
                command.CommandText = commandText;

                if (Transaction != null)
                    command.Transaction = Transaction;

                if (parameters != null)
                {
                    foreach (DbParameter param in parameters)
                        command.Parameters.Add(param);
                }

                using (DbDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        T obj = Activator.CreateInstance<T>();

                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            string fieldName = reader.GetName(i);

                            if (ColumnsPropertiesNamesMapping[objectType].ContainsKey(fieldName))
                            {
                                string propertyName = ColumnsPropertiesNamesMapping[objectType][fieldName];

                                Func<object, object> func = PropertiesConverters[objectType][propertyName][DatabaseType].ConvertToProperty;

                                object readerValue = reader[fieldName];
                                if (!(readerValue is DBNull))
                                    ta[obj, propertyName] = func(readerValue);
                            }
                        }

                        list.Add(obj);
                    }

                    reader.Close();
                }
            }

            return list;
        }

        #endregion
    }
}
