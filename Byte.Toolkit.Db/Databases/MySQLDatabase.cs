﻿using System;
using System.Collections.Generic;
using Byte.Toolkit.Db.Converters;
using Byte.Toolkit.Db.Generators;

namespace Byte.Toolkit.Db.Databases
{
    /// <summary>
    /// MySQL database specifications
    /// </summary>
    public class MySQLDatabase : IDatabase
    {
        private static MySQLDatabase _database = new MySQLDatabase();

        /// <summary>
        /// Static instance
        /// </summary>
        public static MySQLDatabase Instance => _database;

        /// <inheritdoc/>
        public Dictionary<Type, Type> PropertyToColumnMapping => throw new NotImplementedException();

        /// <inheritdoc/>
        public Dictionary<Type, string> ColumnToDbMapping => throw new NotImplementedException();

        /// <inheritdoc/>
        public IEnumerable<IConverter> Converters=> throw new NotImplementedException();

        /// <inheritdoc/>
        public string GenerateDatabaseCreationScript(IEnumerable<MultiDbTable> tables)
        {
            throw new NotImplementedException();
        }
    }
}
