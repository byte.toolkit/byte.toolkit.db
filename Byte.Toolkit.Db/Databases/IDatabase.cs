﻿using System;
using System.Collections.Generic;
using Byte.Toolkit.Db.Converters;
using Byte.Toolkit.Db.Generators;

namespace Byte.Toolkit.Db.Databases
{
    /// <summary>
    /// Database specification interface
    /// </summary>
    public interface IDatabase
    {
        /// <summary>
        /// Mapping between the .NET objects properties types 
        /// and the .NET types of the values returned by the
        /// DataReader
        /// </summary>
        Dictionary<Type, Type> PropertyToColumnMapping { get; }

        /// <summary>
        /// Mapping between the .NET types of the values returned
        /// by the DataReader and the string representation of
        /// the type used by the database
        /// </summary>
        Dictionary<Type, string> ColumnToDbMapping { get; }

        /// <summary>
        /// Converters
        /// </summary>
        IEnumerable<IConverter> Converters { get; }

        /// <summary>
        /// Generate database creation script
        /// </summary>
        /// <param name="tables">Tables to generate</param>
        /// <returns>SQL script</returns>
        string GenerateDatabaseCreationScript(IEnumerable<MultiDbTable> tables);
    }
}
