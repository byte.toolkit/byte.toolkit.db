﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Byte.Toolkit.Db.Converters;
using Byte.Toolkit.Db.Generators;

namespace Byte.Toolkit.Db.Databases
{
    /// <summary>
    /// SqlServer database specifications
    /// </summary>
    public class SqlServerDatabase : IDatabase
    {
        private static readonly SqlServerDatabase _database = new SqlServerDatabase();

        /// <summary>
        /// Static instance
        /// </summary>
        public static SqlServerDatabase Instance => _database;

        private static Dictionary<Type, Type> _propertyToColumnMapping =>
            new Dictionary<Type, Type>()
            {
                { typeof(bool),     typeof(bool)     },
                { typeof(byte),     typeof(byte)     },
                { typeof(byte[]),   typeof(byte[])   },
                { typeof(short),    typeof(short)    },
                { typeof(int),      typeof(int)      },
                { typeof(long),     typeof(long)     },
                { typeof(float),    typeof(float)    },
                { typeof(double),   typeof(double)   },
                { typeof(decimal),  typeof(decimal)  },
                { typeof(string),   typeof(string)   },
                { typeof(char),     typeof(string)   },
                { typeof(DateTime), typeof(DateTime) },
                { typeof(TimeSpan), typeof(TimeSpan) }
            };

        private static Dictionary<Type, string> _columnToDbMapping =>
            new Dictionary<Type, string>()
            {
                { typeof(bool),     "BIT"           },
                { typeof(byte),     "TINYINT"       },
                { typeof(byte[]),   "VARBINARY(50)" },
                { typeof(short),    "SMALLINT"      },
                { typeof(int),      "INT"           },
                { typeof(long),     "BIGINT"        },
                { typeof(float),    "REAL"          },
                { typeof(double),   "FLOAT"         },
                { typeof(decimal),  "DECIMAL(18,0)" },
                { typeof(string),   "NVARCHAR(50)"  },
                { typeof(DateTime), "DATETIME2"     },
                { typeof(TimeSpan), "TIMESTAMP"     }
            };

        /// <inheritdoc/>
        public Dictionary<Type, Type> PropertyToColumnMapping => _propertyToColumnMapping;

        /// <inheritdoc/>
        public Dictionary<Type, string> ColumnToDbMapping => _columnToDbMapping;

        /// <inheritdoc/>
        public IEnumerable<IConverter> Converters=> BuiltinConverters.Converters;

        /// <inheritdoc/>
        public string GenerateDatabaseCreationScript(IEnumerable<MultiDbTable> tables)
        {
            StringBuilder sb = new StringBuilder();

            foreach (MultiDbTable table in tables)
            {
                sb.AppendLine($"CREATE TABLE [{table.TableName}] (");

                int i = 0;
                foreach (MultiDbColumn col in table.Columns)
                {
                    Type propertyType = col.PropertyType;

                    Type colType = PropertyToColumnMapping[propertyType];
                    string dbType = ColumnToDbMapping[colType];
                    string notNull = !col.IsNullable ? " NOT NULL" : "";

                    sb.Append($"    [{col.ColumnName}] {dbType}{notNull}");

                    bool moreItems = (i++ < table.Columns.Count - 1) || (table.Constraints != null && table.Constraints.Count(x => x.InsideTableDeclaration) > 0);

                    sb.AppendLine(moreItems ? "," : "");
                }

                PrimaryKeyConstraint? pk = table.Constraints.OfType<PrimaryKeyConstraint>().FirstOrDefault();
                IEnumerable<ForeignKeyConstraint> fks = table.Constraints.OfType<ForeignKeyConstraint>();

                if (pk != null)
                {
                    sb.Append($"    CONSTRAINT [{pk.ConstraintName}] PRIMARY KEY ([{pk.Columns}])");
                    sb.AppendLine(fks.Count() > 0 ? "," : "");
                }

                i = 0;
                foreach (ForeignKeyConstraint fk in fks)
                {
                    sb.Append($"    CONSTRAINT [{fk.ConstraintName}] FOREIGN KEY ([{fk.Columns}]) REFERENCES [{fk.ForeignTableName}] ([{fk.ForeignColumnName}])");
                    sb.AppendLine(i++ < fks.Count() - 1 ? "," : "");
                }


                sb.AppendLine($");");
                sb.AppendLine();
                sb.AppendLine("GO");
                sb.AppendLine();

                IEnumerable<IndexConstraint> indexes = table.Constraints.OfType<IndexConstraint>();

                foreach (IndexConstraint index in indexes)
                {
                    sb.AppendLine($"CREATE INDEX [{index.ConstraintName}] on [{table.TableName}] ([{index.Columns}]);");
                    sb.AppendLine();
                    sb.AppendLine("GO");
                    sb.AppendLine();
                }

                IEnumerable<UniqueKeyConstraint> uniqueKeys = table.Constraints.OfType<UniqueKeyConstraint>();

                foreach (UniqueKeyConstraint uniqueKey in uniqueKeys)
                {
                    sb.AppendLine($"CREATE UNIQUE INDEX [{uniqueKey.ConstraintName}] on [{table.TableName}] ([{uniqueKey.Columns}]);");
                    sb.AppendLine();
                    sb.AppendLine("GO");
                    sb.AppendLine();
                }
            }

            return sb.ToString();
        }
    }
}
