﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Byte.Toolkit.Db.Converters;
using Byte.Toolkit.Db.Generators;

namespace Byte.Toolkit.Db.Databases
{
    /// <summary>
    /// SQLite database specifications
    /// </summary>
    public class SQLiteDatabase : IDatabase
    {
        private static readonly SQLiteDatabase _database = new SQLiteDatabase();

        /// <summary>
        /// Static instance
        /// </summary>
        public static SQLiteDatabase Instance => _database;

        private static Dictionary<Type, Type> _propertyToColumnMapping =>
            new Dictionary<Type, Type>()
            {
                { typeof(bool),     typeof(long)   },
                { typeof(byte),     typeof(long)   },
                { typeof(byte[]),   typeof(byte[]) },
                { typeof(short),    typeof(long)   },
                { typeof(int),      typeof(long)   },
                { typeof(long),     typeof(long)   },
                { typeof(float),    typeof(double) },
                { typeof(double),   typeof(double) },
                { typeof(decimal),  typeof(string) },
                { typeof(string),   typeof(string) },
                { typeof(char),     typeof(string) },
                { typeof(DateTime), typeof(string) },
                { typeof(TimeSpan), typeof(string) }
            };

        private static Dictionary<Type, string> _columnToDbMapping =>
            new Dictionary<Type, string>()
            {
                { typeof(byte[]), "BLOB"    },
                { typeof(long),   "INTEGER" },
                { typeof(double), "REAL"    },
                { typeof(string), "TEXT"    }
            };

        /// <inheritdoc/>
        public Dictionary<Type, Type> PropertyToColumnMapping => _propertyToColumnMapping;

        /// <inheritdoc/>
        public Dictionary<Type, string> ColumnToDbMapping => _columnToDbMapping;

        /// <inheritdoc/>
        public IEnumerable<IConverter> Converters=> BuiltinConverters.Converters;

        /// <inheritdoc/>
        public string GenerateDatabaseCreationScript(IEnumerable<MultiDbTable> tables)
        {
            StringBuilder sb = new StringBuilder();

            foreach(MultiDbTable table in tables)
            {
                sb.AppendLine($"CREATE TABLE {table.TableName} (");

                int i = 0;
                foreach(MultiDbColumn col in table.Columns)
                {
                    Type propertyType = col.PropertyType;

                    Type colType = PropertyToColumnMapping[propertyType];
                    string dbType = ColumnToDbMapping[colType];
                    string notNull = !col.IsNullable ? " NOT NULL" : "";

                    sb.Append($"    {col.ColumnName} {dbType}{notNull}");

                    bool moreItems = (i++ < table.Columns.Count - 1) || (table.Constraints != null && table.Constraints.Count(x => x.InsideTableDeclaration) > 0);

                    sb.AppendLine(moreItems ? "," : "");
                }

                PrimaryKeyConstraint? pk = table.Constraints.OfType<PrimaryKeyConstraint>().FirstOrDefault();
                IEnumerable<ForeignKeyConstraint> fks = table.Constraints.OfType<ForeignKeyConstraint>();

                if (pk != null)
                {
                    sb.Append($"    PRIMARY KEY ({pk.Columns})");
                    sb.AppendLine(fks.Count() > 0 ? "," : "");
                }

                i = 0;
                foreach(ForeignKeyConstraint fk in fks)
                {
                    sb.Append($"    FOREIGN KEY ({fk.Columns}) REFERENCES {fk.ForeignTableName} ({fk.ForeignColumnName})");
                    sb.AppendLine(i++ < fks.Count() - 1 ? "," : "");
                }
                

                sb.AppendLine($");");
                sb.AppendLine();

                IEnumerable<IndexConstraint> indexes = table.Constraints.OfType<IndexConstraint>();

                foreach(IndexConstraint index in indexes)
                {
                    sb.AppendLine($"CREATE INDEX {index.ConstraintName} on {table.TableName} ({index.Columns});");
                }

                IEnumerable<UniqueKeyConstraint> uniqueKeys = table.Constraints.OfType<UniqueKeyConstraint>();

                foreach(UniqueKeyConstraint uniqueKey in uniqueKeys)
                {
                    sb.AppendLine($"CREATE UNIQUE INDEX {uniqueKey.ConstraintName} on {table.TableName} ({uniqueKey.Columns});");
                }

                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}
