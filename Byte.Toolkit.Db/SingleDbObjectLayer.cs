﻿using System.Collections.Generic;

namespace Byte.Toolkit.Db
{
    /// <summary>
    /// DbObject layer abstract class
    /// </summary>
    /// <typeparam name="T">DbObject type</typeparam>
    public abstract class SingleDbObjectLayer<T>
    {
        /// <summary>
        /// Constructor for <see cref="SingleDbObjectLayer{T}"/>
        /// </summary>
        /// <param name="db"><see cref="SingleDbManager"/> instance</param>
        public SingleDbObjectLayer(SingleDbManager db)
        {
            DbManager = db;
        }

        /// <summary>
        /// DbManager reference
        /// </summary>
        public SingleDbManager DbManager { get; }

        /// <summary>
        /// DbObject queries reference
        /// </summary>
        public Dictionary<string, string> Queries
        {
            get => SingleDbManager.Queries[typeof(T)];
        }
    }
}
