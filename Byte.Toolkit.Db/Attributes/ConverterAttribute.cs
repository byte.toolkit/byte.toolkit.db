﻿using System;
using Byte.Toolkit.Db.Converters;
using Byte.Toolkit.Db.Databases;

namespace Byte.Toolkit.Db.Attributes
{
    /// <summary>
    /// Converter attribute for defining how to convert object property to and from column
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class ConverterAttribute : Attribute
    {
        /// <summary>
        /// Constructor for <see cref="ConverterAttribute"/>
        /// </summary>
        /// <param name="databaseType">Database type</param>
        /// <param name="converterType">Converter type</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        public ConverterAttribute(Type databaseType, Type converterType)
        {
            if (databaseType == null)
                throw new ArgumentNullException(nameof(databaseType));
            if (converterType == null)
                throw new ArgumentNullException(nameof(converterType));

            if (!typeof(IDatabase).IsAssignableFrom(databaseType))
                throw new InvalidOperationException($"databaseType must implement '{typeof(IDatabase)}' interface");
            if (!typeof(IConverter).IsAssignableFrom(converterType))
                throw new InvalidOperationException($"converterType must implement '{typeof(IConverter)}' interface");

            DatabaseType = databaseType;
            ConverterType = converterType;
        }

        /// <summary>
        /// Database type. Must implement <see cref="IDatabase"/> interface
        /// </summary>
        public Type DatabaseType { get; set; }

        /// <summary>
        /// Converter type. Must implement <see cref="IConverter"/> interface
        /// </summary>
        public Type ConverterType { get; set; }
    }
}
