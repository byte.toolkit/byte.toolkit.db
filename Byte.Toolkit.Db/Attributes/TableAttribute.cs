﻿using System;

namespace Byte.Toolkit.Db.Attributes
{
    /// <summary>
    /// Table attribute to specify that an object can be used with
    /// <see cref="SingleDbManager"/>, <see cref="MultiDbManager"/> and <see cref="MultiDbManager{TDatabase}"/>
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class TableAttribute : Attribute
    {
        /// <summary>
        /// Constructor for <see cref="TableAttribute"/>
        /// </summary>
        /// <param name="name">Table name</param>
        public TableAttribute(string? name = null)
        {
            Name = name;
        }

        /// <summary>
        /// Table name
        /// </summary>
        public string? Name { get; }
    }
}
