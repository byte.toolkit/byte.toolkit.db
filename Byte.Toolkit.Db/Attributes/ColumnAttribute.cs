﻿using System;

namespace Byte.Toolkit.Db.Attributes
{
    /// <summary>
    /// Column attribute for mapping between column and object property
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ColumnAttribute : Attribute
    {
        /// <summary>
        /// Constructor for <see cref="ColumnAttribute"/>
        /// </summary>
        /// <param name="name">Column name</param>
        public ColumnAttribute(string? name = null)
        {
            Name = name;
        }

        /// <summary>
        /// Column name
        /// </summary>
        public string? Name { get; }
    }
}
