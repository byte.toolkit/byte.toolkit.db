﻿namespace Byte.Toolkit.Db
{
    /// <summary>
    /// Utils class
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// Get field name from property name
        /// </summary>
        /// <param name="propertyName">Property name</param>
        /// <returns>Field name</returns>
        public static string GetFieldname(string propertyName)
        {
            return $"_{propertyName[0].ToString().ToLower()}{propertyName.Substring(1)}";
        }

        /// <summary>
        /// Get parameter name from property name
        /// </summary>
        /// <param name="propertyName">Property name</param>
        /// <returns>Parameter name</returns>
        public static string GetParameterName(string propertyName)
        {
            return $"{propertyName[0].ToString().ToLower()}{propertyName.Substring(1)}";
        }

        /// <summary>
        /// Get class instance name
        /// </summary>
        /// <param name="className">Class name</param>
        /// <returns>Instance name</returns>
        public static string GetInstanceName(string className)
        {
            return $"{className[0].ToString().ToLower()}{className.Substring(1)}";
        }
    }
}
