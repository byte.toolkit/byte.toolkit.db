﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Xml;
using Byte.Toolkit.Db.Attributes;
using Byte.Toolkit.Db.Converters;
using FastMember;

namespace Byte.Toolkit.Db
{
    /// <summary>
    /// Database management class.
    /// Registers and caches all the informations needed
    /// by <see cref="MultiDbManager{TDatabase}"/>
    /// </summary>
    public class MultiDbManager : SingleDbManager
    {
        #region Constructors

        static MultiDbManager()
        {
            DatabasesTypes = new List<Type>();
            TypeAccessors = new Dictionary<Type, TypeAccessor>();
            ColumnsPropertiesNamesMapping = new Dictionary<Type, Dictionary<string, string>>();
            PropertiesConverters = new Dictionary<Type, Dictionary<string, Dictionary<Type, IConverter>>>();
            Queries = new Dictionary<Type, Dictionary<string, string>>();
        }

#if NETSTANDARD2_0
        /// <summary>
        /// Constructor for <see cref="MultiDbManager"/>
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <param name="factory">Database provider factory</param>
        public MultiDbManager(string connectionString, DbProviderFactory factory)
            : base(connectionString, factory)
        { }
#elif (NETSTANDARD2_1_OR_GREATER || NET461_OR_GREATER)
        /// <summary>
        /// Constructor for <see cref="MultiDbManager"/>
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <param name="provider">Database provider</param>
        public MultiDbManager(string connectionString, string provider)
            : base(connectionString, provider)
        { }
#endif

        #endregion

        #region Properties

        /// <summary>
        /// Registered databases types
        /// </summary>
        public static List<Type> DatabasesTypes { get; }

        /// <summary>
        /// Types accessors
        /// </summary>
        new public static Dictionary<Type, TypeAccessor> TypeAccessors { get; set; }

        /// <summary>
        /// Types column-property mapping
        /// </summary>
        new public static Dictionary<Type, Dictionary<string, string>> ColumnsPropertiesNamesMapping { get; set; }

        /// <summary>
        /// Queries
        /// </summary>
        new public static Dictionary<Type, Dictionary<string, string>> Queries { get; private set; }

        /// <summary>
        /// Properties converters
        /// </summary>
        public static Dictionary<Type, Dictionary<string, Dictionary<Type, IConverter>>> PropertiesConverters { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Register database object type
        /// </summary>
        /// <param name="objectType">Database object type</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="MissingTableAttributeException"></exception>
        /// <exception cref="DbObjectAlreadyRegisteredException"></exception>
        new public static void RegisterDbObjectType(Type objectType)
        {
            if (objectType == null)
                throw new ArgumentNullException(nameof(objectType));
            if (objectType.GetCustomAttribute<TableAttribute>() == null)
                throw new MissingTableAttributeException(objectType);
            if (TypeAccessors.ContainsKey(objectType) || ColumnsPropertiesNamesMapping.ContainsKey(objectType))
                throw new DbObjectAlreadyRegisteredException(objectType);

            // Create type accessors
            TypeAccessors.Add(objectType, TypeAccessor.Create(objectType));

            // Initialize mapping and converters with object type
            ColumnsPropertiesNamesMapping.Add(objectType, new Dictionary<string, string>());
            PropertiesConverters.Add(objectType, new Dictionary<string, Dictionary<Type, IConverter>>());

            foreach (PropertyInfo prop in objectType.GetProperties())
            {
                ColumnAttribute? attr = prop.GetCustomAttribute<ColumnAttribute>();

                // If the property has a [Column] attribute
                if (attr != null)
                {
                    string columnName = attr.Name ?? prop.Name;
                    ColumnsPropertiesNamesMapping[objectType].Add(columnName, prop.Name);
                    PropertiesConverters[objectType].Add(prop.Name, new Dictionary<Type, IConverter>());

                    // Get the property converters
                    IEnumerable<ConverterAttribute> convertersAttrs = prop.GetCustomAttributes<ConverterAttribute>();

                    foreach(Type databaseType in DatabasesTypes)
                    {
                        // Try to get a converter for the database type
                        ConverterAttribute? converterAttr = convertersAttrs.FirstOrDefault(x => x.DatabaseType == databaseType);

                        // If the converter has been found register the converter
                        if (converterAttr != null)
                        {
                            IConverter converter = (IConverter)Activator.CreateInstance(converterAttr.ConverterType);
                            PropertiesConverters[objectType][prop.Name].Add(databaseType, converter);
                        }
                        else
                            PropertiesConverters[objectType][prop.Name].Add(databaseType, BuiltinConverters.NoConversionConverter);
                    }
                }
            }
        }

        /// <summary>
        /// Register databases types
        /// </summary>
        /// <param name="databasesTypes">Databases types</param>
        /// <exception cref="DatabaseAlreadyRegisteredException"></exception>
        public static void RegisterDatabasesTypes(params Type[] databasesTypes)
        {
            foreach(Type databaseType in databasesTypes)
            {
                if (DatabasesTypes.Contains(databaseType))
                    throw new DatabaseAlreadyRegisteredException(databaseType);

                DatabasesTypes.Add(databaseType);
            }
        }

        /// <summary>
        /// Register database object types from assemblies
        /// </summary>
        /// <param name="assemblies">Assemblies to scan</param>
        new public static void RegisterDbObjectTypes(params Assembly[] assemblies)
        {
            foreach (Assembly assembly in assemblies)
            {
                Type[] types = assembly.GetTypes();

                foreach (Type type in types)
                {
                    TableAttribute? attr = type.GetCustomAttribute<TableAttribute>();

                    if (attr != null)
                        RegisterDbObjectType(type);
                }
            }
        }

        /// <summary>
        /// Add queries for a DbObject
        /// </summary>
        /// <param name="objectType">DbObject type</param>
        /// <param name="queries">Queries</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ObjectNotRegisteredException"></exception>
        new public static void AddQueries(Type objectType, Dictionary<string, string> queries)
        {
            if (objectType == null)
                throw new ArgumentNullException(nameof(objectType));
            if (queries == null)
                throw new ArgumentNullException(nameof(queries));
            if (!TypeAccessors.ContainsKey(objectType))
                throw new ObjectNotRegisteredException(objectType);

            if (!Queries.ContainsKey(objectType))
                Queries.Add(objectType, queries);
            else
                Queries[objectType] = queries;
        }

        /// <summary>
        /// Add queries file for a DbObject
        /// </summary>
        /// <param name="objectType">DbObject type</param>
        /// <param name="file">Query file</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ObjectNotRegisteredException"></exception>
        /// <exception cref="InvalidQueriesFileException"></exception>
        new public static void AddQueriesFile(Type objectType, string file)
        {
            if (objectType == null)
                throw new ArgumentNullException(nameof(objectType));
            if (file == null)
                throw new ArgumentNullException(nameof(file));
            if (!TypeAccessors.ContainsKey(objectType))
                throw new ObjectNotRegisteredException(objectType);

            XmlDocument doc = new XmlDocument();
            doc.Load(file);

            Queries.Add(objectType, new Dictionary<string, string>());

            XmlNodeList requestList = doc.SelectNodes("/Queries/Query");

            foreach (XmlNode queryNode in requestList)
            {
                XmlAttribute nameAttr = queryNode.Attributes["Name"];

                if (nameAttr == null)
                    throw new InvalidQueriesFileException("Name attribute missing on a Query node");

                Queries[objectType].Add(nameAttr.Value, queryNode.InnerText);
            }
        }

        #endregion
    }
}
