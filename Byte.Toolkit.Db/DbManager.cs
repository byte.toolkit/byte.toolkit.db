﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace Byte.Toolkit.Db
{
    /// <summary>
    /// Database management class. Contains the base elements
    /// for interaction with a database
    /// </summary>
    public class DbManager : IDisposable
    {
        #region Constructors

#if NETSTANDARD2_0
        /// <summary>
        /// Constructor for <see cref="DbManager"/>
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <param name="factory">Database provider factory</param>
        public DbManager(string connectionString, DbProviderFactory factory)
        {
            Factory = factory;
            Connection = Factory.CreateConnection();
            Connection.ConnectionString = connectionString;
        }
#elif (NETSTANDARD2_1_OR_GREATER || NET461_OR_GREATER)
        /// <summary>
        /// Constructor for <see cref="DbManager"/>
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <param name="provider">Database provider</param>
        public DbManager(string connectionString, string provider)
        {
            Factory = DbProviderFactories.GetFactory(provider);
            Connection = Factory.CreateConnection();
            Connection.ConnectionString = connectionString;
        }
#endif

        /// <summary>
        /// Destructor for <see cref="DbManager"/>
        /// </summary>
        ~DbManager()
        {
            Dispose(false);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Database provider factory
        /// </summary>
        public DbProviderFactory Factory { get; private set; }

        /// <summary>
        /// Database connection
        /// </summary>
        public DbConnection Connection { get; private set; }

        /// <summary>
        /// Database transaction
        /// </summary>
        public DbTransaction? Transaction { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// Open a database connection
        /// </summary>
        /// <exception cref="ObjectDisposedException"></exception>
        public void Open()
        {
            if (_disposed)
                throw new ObjectDisposedException(typeof(DbManager).FullName);

            Connection.Open();
        }

        /// <summary>
        /// Close the database connection
        /// </summary>
        /// <exception cref="ObjectDisposedException"></exception>
        public void Close()
        {
            if (_disposed)
                throw new ObjectDisposedException(typeof(DbManager).FullName);

            Connection.Close();
        }

        /// <summary>
        /// Start a database Transaction
        /// </summary>
        /// <exception cref="ObjectDisposedException"></exception>
        public void BeginTransaction()
        {
            if (_disposed)
                throw new ObjectDisposedException(typeof(DbManager).FullName);

            Transaction = Connection.BeginTransaction();
        }

        /// <summary>
        /// End the database transaction with a commit or a rollback
        /// </summary>
        /// <param name="commit">Commit or Rollback</param>
        /// <exception cref="ObjectDisposedException"></exception>
        public void EndTransaction(bool commit)
        {
            if (_disposed)
                throw new ObjectDisposedException(typeof(DbManager).FullName);

            if (commit)
                Transaction?.Commit();
            else
                Transaction?.Rollback();

            Transaction?.Dispose();
            Transaction = null;
        }

        /// <summary>
        /// Return a new parameter
        /// </summary>
        /// <returns>DbParameter</returns>
        /// <exception cref="ObjectDisposedException"></exception>
        public DbParameter CreateParameter()
        {
            if (_disposed)
                throw new ObjectDisposedException(typeof(DbManager).FullName);

            return Factory.CreateParameter();
        }

        /// <summary>
        /// Return a new parameter
        /// </summary>
        /// <param name="name">Parameter name</param>
        /// <param name="value">Parameter value</param>
        /// <param name="paramDirection">Parameter direction</param>
        /// <returns>DbParameter</returns>
        /// <exception cref="ObjectDisposedException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public virtual DbParameter CreateParameter(string name, object? value, ParameterDirection paramDirection = ParameterDirection.Input)
        {
            if (_disposed)
                throw new ObjectDisposedException(typeof(DbManager).FullName);
            if (name == null)
                throw new ArgumentNullException(nameof(name));

            DbParameter param = Factory.CreateParameter();
            param.ParameterName = name;
            param.Value = value ?? DBNull.Value;
            param.Direction = paramDirection;
            return param;
        }

        /// <summary>
        /// Fill a DataTable with a query
        /// </summary>
        /// <param name="table">DataTable to fill</param>
        /// <param name="commandText">Command text</param>
        /// <param name="commandType">Command type</param>
        /// <param name="parameters">Command parameters</param>
        /// <exception cref="ObjectDisposedException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public void FillDataTable(DataTable table, string commandText, CommandType commandType = CommandType.Text, IEnumerable<DbParameter>? parameters = null)
        {
            if (_disposed)
                throw new ObjectDisposedException(typeof(DbManager).FullName);
            if (table == null)
                throw new ArgumentNullException(nameof(table));
            if (commandText == null)
                throw new ArgumentNullException(nameof(commandText));

            using (DbCommand command = Connection.CreateCommand())
            {
                command.CommandType = commandType;
                command.CommandText = commandText;

                if (Transaction != null)
                    command.Transaction = Transaction;

                if (parameters != null)
                {
                    foreach (DbParameter param in parameters)
                        command.Parameters.Add(param);
                }

                using (DbDataAdapter adapter = Factory.CreateDataAdapter())
                {
                    adapter.SelectCommand = command;
                    adapter.Fill(table);
                }
            }
        }

        /// <summary>
        /// Execute a SQL statement
        /// </summary>
        /// <param name="commandText">Command text</param>
        /// <param name="commandType">Command type</param>
        /// <param name="parameters">Command parameters</param>
        /// <returns>The number of rows affected</returns>
        /// <exception cref="ObjectDisposedException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public int ExecuteNonQuery(string commandText, CommandType commandType = CommandType.Text, IEnumerable<DbParameter>? parameters = null)
        {
            if (_disposed)
                throw new ObjectDisposedException(typeof(DbManager).FullName);
            if (commandText == null)
                throw new ArgumentNullException(nameof(commandText));

            using (DbCommand command = Connection.CreateCommand())
            {
                command.CommandType = commandType;
                command.CommandText = commandText;

                if (Transaction != null)
                    command.Transaction = Transaction;

                if (parameters != null)
                {
                    foreach (DbParameter param in parameters)
                        command.Parameters.Add(param);
                }

                return command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Execute a query and returns the first column of the first row
        /// </summary>
        /// <param name="commandText">Command text</param>
        /// <param name="commandType">Command type</param>
        /// <param name="parameters">Command parameters</param>
        /// <returns>First column of the first row</returns>
        /// <exception cref="ObjectDisposedException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public object ExecuteScalar(string commandText, CommandType commandType = CommandType.Text, IEnumerable<DbParameter>? parameters = null)
        {
            if (_disposed)
                throw new ObjectDisposedException(typeof(DbManager).FullName);
            if (commandText == null)
                throw new ArgumentNullException(nameof(commandText));

            using (DbCommand command = Connection.CreateCommand())
            {
                command.CommandType = commandType;
                command.CommandText = commandText;

                if (Transaction != null)
                    command.Transaction = Transaction;

                if (parameters != null)
                {
                    foreach (DbParameter param in parameters)
                        command.Parameters.Add(param);
                }

                return command.ExecuteScalar();
            }
        }

        /// <summary>
        /// Get the list of the columns names and types
        /// </summary>
        /// <param name="commandText">Command text</param>
        /// <param name="commandType">Command type</param>
        /// <param name="parameters">Command parameters</param>
        /// <returns>List of the columns and types</returns>
        /// <exception cref="ObjectDisposedException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public Dictionary<string, Type> GetColumnsNamesAndTypes(string commandText, CommandType commandType = CommandType.Text, IEnumerable<DbParameter>? parameters = null)
        {
            if (_disposed)
                throw new ObjectDisposedException(typeof(DbManager).FullName);
            if (commandText == null)
                throw new ArgumentNullException(nameof(commandText));

            Dictionary<string, Type> dic = new Dictionary<string, Type>();

            DataTable dt = new DataTable();
            FillDataTable(dt, commandText, commandType, parameters);

            foreach (DataColumn col in dt.Columns)
                dic.Add(col.ColumnName, col.DataType);

            return dic;
        }

        #endregion

        #region IDisposable members

        /// <summary>
        /// Is object disposed
        /// </summary>
        protected bool _disposed;

        /// <summary>
        /// Releases all resources used
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases all resources used
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                Connection?.Dispose();
                Transaction?.Dispose();
            }

            _disposed = true;
        }

        #endregion
    }
}
